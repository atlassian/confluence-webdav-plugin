# THIS REPOSITORY HAS BEEN FROZEN AND MIGRATED

This repository has been migrated to the confluence-public-plugins repository. This is part of the developer productivity team's efforts in bringing homogenous plugins closer together and allowing for overarching changes to be easier to manage. Please checkout the new repository in order to start working on this plugin.

* New home: [confluence-public-plugins](https://bitbucket.org/atlassian/confluence-public-plugins/src/master/) 
* Project status: [Confluence Monorepo Plugin Status](https://hello.atlassian.net/wiki/spaces/CSD/pages/2786862612/Confluence+Monorepo+plugin+migration+status) 
* Project overview: [Move closer to Confluence DC monorepo and colocation of tests](https://hello.atlassian.net/wiki/spaces/CSD/pages/2641350532/Move+closer+to+Confluence+DC+monorepo+and+colocation+of+tests) 
* Contact for help: Contact person on Project status page or [#conf-dc-thunderbird](https://atlassian.slack.com/archives/C01KJLNDB1D)

# Confluence WebDAV Plugin

Adds WebDAV support to Confluence. Provides a `<davResourceFactory>` module type other plugins may use to publish WebDAV resources. Also ships with a default resource factory that exposes Confluence spaces / content / revisions / attachments as WebDAV resources.

### Builds

Located in [CONFPLGTRK-WBDVTRUNK](https://server-syd-bamboo.internal.atlassian.com/browse/CBP-WBDVTRUNK) on Bamboo.

### Ownership

This plugin does not have a clear owner. PRs reviewers can be selected from the fortunate few who most recently touched this plugin.

Setup the following properties when running in IDE,

-Dwebdav.host=localhost -Dhttp.port=8080 -Dwebdav.port=8080 -Dwebdav.context=confluence -Dwebdav.resource.path.prefix=/plugins/servlet/confluence/default
