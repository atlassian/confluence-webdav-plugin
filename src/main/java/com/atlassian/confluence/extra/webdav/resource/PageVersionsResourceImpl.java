package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Represents the <code>&amp;#064;versions</code> directory in Confluence pages.
 *
 * @author weiching.cher
 */
public class PageVersionsResourceImpl extends AbstractVersionsResource {
    private final String spaceKey;

    private final String pageTitle;

    public PageVersionsResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            WebdavSettingsManager webdavSettingsManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, webdavSettingsManager, pageManager);
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public ContentEntityObject getContentEntityObject() {
        return getPageManager().getPage(spaceKey, pageTitle);
    }

    public void addMember(DavResource davResource, InputContext inputContext) throws DavException {
        String[] pathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = pathComponents[pathComponents.length - 1];
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

        if (StringUtils.equals(GeneratedResourceReadMeResource.DISPLAY_NAME, resourceName)) {
            resourceStates.unhideContentVersionsReadme(getContentEntityObject());
        } else {
            resourceStates.unhideContentVersionText(getContentEntityObject(), resourceName);
        }
    }

    public void removeMember(DavResource davResource) throws DavException {
        String[] pathComponents = StringUtils.split(davResource.getResourcePath(), '/');
        String resourceName = pathComponents[pathComponents.length - 1];
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

        if (StringUtils.equals(GeneratedResourceReadMeResource.DISPLAY_NAME, resourceName)) {
            resourceStates.hideContentVersionsReadme(getContentEntityObject());
        } else {
            resourceStates.hideContentVersionText(getContentEntityObject(), resourceName);
        }
    }


    protected Collection<DavResource> getMemberResources() {
        try {
            List<DavResource> members = new ArrayList<DavResource>();
            DavResourceFactory davResourceFactory = getFactory();
            DavResourceLocator locator = getLocator();

            for (DavResourceLocator versionContentResourceLocator : getVersionContentResourceLocators())
                members.add(davResourceFactory.createResource(versionContentResourceLocator, getSession()));

            members.add(
                    davResourceFactory.createResource(
                            locator.getFactory().createResourceLocator(
                                    locator.getPrefix(),
                                    locator.getWorkspacePath(),
                                    getParentResourcePath()
                                            + "/" + AbstractVersionsResource.DISPLAY_NAME
                                            + "/" + GeneratedResourceReadMeResource.DISPLAY_NAME,
                                    false
                            ), getSession()
                    )
            );

            return members;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }

    private DavResourceLocator[] getVersionContentResourceLocators() {
        DavResourceLocator locator = getLocator();
        ContentEntityObject contentEntityObject = getContentEntityObject();
        List<VersionHistorySummary> versionHistorySummaries = getPageManager().getVersionHistorySummaries(contentEntityObject);
        StringBuffer contentPathBuffer = new StringBuffer();
        List<DavResourceLocator> pageResourceLocators = new ArrayList<DavResourceLocator>();
        String parentResourcePath = getParentResourcePath();

        for (VersionHistorySummary versionHistorySummary : versionHistorySummaries) {
            contentPathBuffer.setLength(0);
            contentPathBuffer.append(parentResourcePath)
                    .append('/').append(AbstractVersionsResource.DISPLAY_NAME)
                    .append('/').append(AbstractVersionContentResource.DISPLAY_NAME_PREFIX).append(versionHistorySummary.getVersion()).append(AbstractVersionContentResource.DISPLAY_NAME_SUFFIX);

            DavResourceLocator pageResourceLocator = locator.getFactory().createResourceLocator(
                    locator.getPrefix(),
                    locator.getWorkspacePath(),
                    contentPathBuffer.toString(),
                    false
            );

            pageResourceLocators.add(pageResourceLocator);
        }

        return pageResourceLocators.toArray(new DavResourceLocator[pageResourceLocators.size()]);
    }
}
