package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.extra.flyingpdf.PdfExporterService;
import com.atlassian.confluence.extra.webdav.job.ContentJobQueue;
import com.atlassian.confluence.extra.webdav.resource.AbstractExportsResource;
import com.atlassian.confluence.extra.webdav.resource.AbstractVersionsResource;
import com.atlassian.confluence.extra.webdav.resource.BlogPostContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsDayResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsMonthResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsYearResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.DashboardResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.GeneratedResourceReadMeResource;
import com.atlassian.confluence.extra.webdav.resource.GlobalSpacesResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.NonExistentResource;
import com.atlassian.confluence.extra.webdav.resource.PageAttachmentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageExportsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PagePdfExportContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageUrlResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageVersionContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageVersionsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageWordExportContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PersonalSpacesResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceAttachmentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.WorkspaceResourceImpl;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.servlet.download.SafeContentHeaderGuesser;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This classes parses request URIs and returns appropriate implementations of
 * {@link com.atlassian.confluence.extra.webdav.resource.AbstractConfluenceResource} to represent the requested
 * resource.
 *
 * @author weiching.cher
 */
@Component
public class ConfluenceResourceFactory implements DavResourceFactory {
    private static final Pattern VERSION_NUMBER_PATTERN = Pattern.compile("[a-zA-Z] *(\\d+)\\.txt");

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfluenceResourceFactory.class);

    private final String workspaceName;

    private final ContextPathHolder contextPathHolder;

    private final UserAccessor userAccessor;

    private final PdfExporterService pdfExporterService;

    private final SettingsManager settingsManager;

    private final WebdavSettingsManager webdavSettingsManager;

    private final PermissionManager permissionManager;

    private final SpaceManager spaceManager;

    private final PageManager pageManager;

    private final XhtmlContent xhtmlContent;

    private final AttachmentManager attachmentManager;

    private final SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser;

    private final ContentJobQueue contentJobQueue;

    @Autowired
    public ConfluenceResourceFactory(
            @ComponentImport ContextPathHolder contextPathHolder,
            @ComponentImport UserAccessor userAccessor,
            @ComponentImport PdfExporterService pdfExporterService,
            @ComponentImport SettingsManager settingsManager,
            WebdavSettingsManager webdavSettingsManager,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport XhtmlContent xhtmlContent,
            @ComponentImport AttachmentManager attachmentManager,
            @ComponentImport SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser,
            ContentJobQueue contentJobQueue) {
        this.workspaceName = "default";
        this.contextPathHolder = contextPathHolder;
        this.userAccessor = userAccessor;
        this.pdfExporterService = pdfExporterService;
        this.settingsManager = settingsManager;
        this.webdavSettingsManager = webdavSettingsManager;
        this.permissionManager = permissionManager;
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
        this.xhtmlContent = xhtmlContent;
        this.attachmentManager = attachmentManager;
        this.attachmentSafeContentHeaderGuesser = attachmentSafeContentHeaderGuesser;
        this.contentJobQueue = contentJobQueue;
    }

    private DavSession getDavSession(final HttpServletRequest httpServletRequest) {
        return (DavSession) httpServletRequest.getSession().getAttribute(ConfluenceDavSession.class.getName());
    }

    public DavResource createResource(DavResourceLocator davResourceLocator, DavServletRequest davServletRequest,
                                      DavServletResponse davServletResponse) throws DavException {
        return createResource(davResourceLocator, getDavSession(davServletRequest));
    }

    public DavResource createResource(DavResourceLocator davResourceLocator, DavSession davSession) throws DavException {
        String resourcePath = davResourceLocator.getResourcePath();
        LOGGER.debug("Trying to locate DavResource: " + resourcePath);

        ConfluenceDavSession confluenceDavSession = (ConfluenceDavSession) davSession;
        LockManager lockManager = confluenceDavSession.getLockManager();

        String[] resourcePathTokens = StringUtils.split(resourcePath, '/');

        if (StringUtils.isBlank(davResourceLocator.getWorkspacePath())) {
            return new WorkspaceResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession, workspaceName);
        }

        if (isPathPointingToDashboard(resourcePathTokens)) {
            /* /workspaceName/ */
            return new DashboardResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession, workspaceName);
        } else if (isPathPointingToEitherGlobalOrPersonalSpaces(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/ */
            if (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])) {
                return new GlobalSpacesResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession, spaceManager);
            } else if (StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])) {
                return new PersonalSpacesResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession, spaceManager);
            } else {
                return new NonExistentResource(davResourceLocator, this, lockManager, confluenceDavSession);
            }
        } else if (isPathPointingToSpace(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/ */
            return new SpaceResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    permissionManager, spaceManager, pageManager, attachmentManager,
                    resourcePathTokens[resourcePathTokens.length - 1]
            );
        } else if (isPathPointingToSpaceDescription(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Space Name>.txt */
            return new SpaceContentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    settingsManager, spaceManager,
                    resourcePathTokens[2]);
        } else if (isPathPointingSpaceAttachment(resourcePathTokens)) {
            return new SpaceAttachmentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    spaceManager, attachmentSafeContentHeaderGuesser, attachmentManager,
                    resourcePathTokens[2], resourcePathTokens[3], confluenceDavSession.getUserAgent()
            );
        } else if (isPathPointingToPage(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/ */
            return new PageResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    permissionManager, spaceManager, pageManager, attachmentManager, contentJobQueue,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 1]
            );
        } else if (isPathPointingToPageContent(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/<Page Title>.txt */
            return new PageContentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    settingsManager, pageManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 2]
            );
        } else if (isPathPointingToPageUrl(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/<Page Title>.url */
            return new PageUrlResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    settingsManager, webdavSettingsManager, pageManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 2]
            );
        } else if (isPathPointingToPageAttachment(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/<Attachment File Name> */
            return new PageAttachmentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    permissionManager, pageManager, attachmentSafeContentHeaderGuesser, attachmentManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 2], resourcePathTokens[resourcePathTokens.length - 1],
                    confluenceDavSession.getUserAgent()
            );
        } else if (isPathPointingToPageVersions(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@versions/ */
            return new PageVersionsResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession,
                    webdavSettingsManager, pageManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 2]);
        } else if (isPathPointingToPageVersion(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@version/Version/<Version Number>.txt */
            String versionFile = resourcePathTokens[resourcePathTokens.length - 1];
            Matcher versionMatcher = VERSION_NUMBER_PATTERN.matcher(versionFile);

            versionMatcher.find();

            return new PageVersionContentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    settingsManager, pageManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 3], Integer.parseInt(versionMatcher.group(1))
            );
        } else if (isPathPointingToPageVersionsReadme(resourcePathTokens)) {
            /* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@version/Version/README.txt */
            return new GeneratedResourceReadMeResource(davResourceLocator, this, lockManager, confluenceDavSession, settingsManager);
        } else if (isPathPointingToPageExports(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@exports/ */
            return new PageExportsResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    webdavSettingsManager, pageManager,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 2]);
        } else if (isPathPointingToPageWordExport(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@exports/<Page Title>.doc */
            return new PageWordExportContentResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession,
                    contextPathHolder, settingsManager, pageManager, xhtmlContent,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 3]);
        } else if (isPathPointingToPagePdfExport(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@exports/<Page Title>.pdf */
            return new PagePdfExportContentResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    pageManager, pdfExporterService,
                    resourcePathTokens[2], resourcePathTokens[resourcePathTokens.length - 3]);
        } else if (isPathPointingToPageExportsReadme(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/<Page Title>/@exports/README.txt */
            return new GeneratedResourceReadMeResource(davResourceLocator, this, lockManager, confluenceDavSession, settingsManager);
        } else if (isPathPointingToBlog(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/@news/ */
            return new BlogPostsResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    permissionManager, spaceManager, pageManager,
                    resourcePathTokens[2]);
        } else if (isPathPointingToBlogYear(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/@news/yyyy/ */
            return new BlogPostsYearResourceImpl(
                    davResourceLocator, this, lockManager, confluenceDavSession,
                    permissionManager, spaceManager, pageManager,
                    resourcePathTokens[2], new Integer(resourcePathTokens[4]));
        } else if (isPathPointingToBlogMonth(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/@news/yyyy/MM/ */
            return new BlogPostsMonthResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession,
                    userAccessor, permissionManager, spaceManager, pageManager,
                    resourcePathTokens[2], new Integer(resourcePathTokens[4]), new Integer(resourcePathTokens[5]));
        } else if (isPathPointingToBlogDay(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/@news/yyyy/MM/dd/ */
            return new BlogPostsDayResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession,
                    userAccessor, permissionManager, pageManager, spaceManager,
                    resourcePathTokens[2], new Integer(resourcePathTokens[4]), new Integer(resourcePathTokens[5]), new Integer(resourcePathTokens[6]));
        } else if (isPathPointingToBlogPostContent(resourcePathTokens)) {
        	/* /workspace/<Global|Personal>/<Space Key>/@news/yyyy/MM/dd/<Blog Title>.txt */
            return new BlogPostContentResourceImpl(davResourceLocator, this, lockManager, confluenceDavSession,
                    settingsManager, userAccessor, pageManager,
                    resourcePathTokens[2], new Integer(resourcePathTokens[4]), new Integer(resourcePathTokens[5]), new Integer(resourcePathTokens[6]),
                    resourcePathTokens[7].substring(0, resourcePathTokens[7].length() - BlogPostContentResourceImpl.DISPLAY_NAME_SUFFIX.length()));
        } else {
            return new NonExistentResource(davResourceLocator, this, lockManager, confluenceDavSession);
        }
    }

    private boolean hasAliasInTokens(String[] tokens, int startIndex) {
        for (int i = startIndex; i < tokens.length; ++i) {
            if (tokens[i].indexOf('@') >= 0)
                return true;
        }

        return false;
    }

    private boolean areTokensNumeric(String[] tokens, int lowerOffset, int upperOffset) {
        int limit = Math.min(upperOffset, tokens.length);

        for (int i = lowerOffset; i < limit; ++i) {
            String token = tokens[i];
            if (StringUtils.isBlank(token) || !StringUtils.isNumeric(token))
                return false;
        }

        return true;
    }

    private boolean isPathPointingToBlogPostContent(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 8
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            return null != space
                    && StringUtils.equals(BlogPostsResourceImpl.DISPLAY_NAME, resourcePathTokens[3])
                    && areTokensNumeric(resourcePathTokens, 4, resourcePathTokens.length - 1)
                    && resourcePathTokens[7].endsWith(BlogPostContentResourceImpl.DISPLAY_NAME_SUFFIX)
                    && resourcePathTokens[7].length() > BlogPostContentResourceImpl.DISPLAY_NAME_SUFFIX.length();
        }

        return false;
    }

    private boolean isPathPointingToBlogDay(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 7
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            return null != space
                    && StringUtils.equals(BlogPostsResourceImpl.DISPLAY_NAME, resourcePathTokens[3])
                    && areTokensNumeric(resourcePathTokens, 4, resourcePathTokens.length);
        }

        return false;
    }

    private boolean isPathPointingToBlogMonth(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            return null != space
                    && StringUtils.equals(BlogPostsResourceImpl.DISPLAY_NAME, resourcePathTokens[3])
                    && areTokensNumeric(resourcePathTokens, 4, resourcePathTokens.length);
        }

        return false;
    }

    private boolean isPathPointingToBlogYear(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            return null != space
                    && StringUtils.equals(BlogPostsResourceImpl.DISPLAY_NAME, resourcePathTokens[3])
                    && areTokensNumeric(resourcePathTokens, 4, resourcePathTokens.length);
        }

        return false;
    }

    private boolean isPathPointingToBlog(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 4
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            return null != space
                    && StringUtils.equals(BlogPostsResourceImpl.DISPLAY_NAME, resourcePathTokens[3]);
        }

        return false;
    }

    private boolean isPathPointingToPageWordExport(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 3]);

            return null != space
                    && StringUtils.equals(AbstractExportsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 2])
                    && null != page
                    && resourcePathTokens[resourcePathTokens.length - 1].endsWith(PageWordExportContentResourceImpl.DISPLAY_NAME_SUFFIX);
        }

        return false;
    }

    private boolean isPathPointingToPagePdfExport(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 3]);

            return null != space
                    && StringUtils.equals(AbstractExportsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 2])
                    && null != page
                    && resourcePathTokens[resourcePathTokens.length - 1].endsWith(PagePdfExportContentResourceImpl.DISPLAY_NAME_SUFFIX);
        }

        return false;
    }

    private boolean isPathPointingToPageExportsReadme(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 3]);

            return null != space
                    && StringUtils.equals(AbstractExportsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 2])
                    && null != page
                    && StringUtils.equals(resourcePathTokens[resourcePathTokens.length - 1], GeneratedResourceReadMeResource.DISPLAY_NAME);
        }

        return false;
    }

    private boolean isPathPointingToPageExports(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 2]);

            return null != space
                    && StringUtils.equals(AbstractExportsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 1])
                    && null != page;
        }

        return false;
    }

    private boolean isPathPointingToPageVersion(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 3]);

            return null != space
                    && StringUtils.equals(AbstractVersionsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 2])
                    && null != page
                    && VERSION_NUMBER_PATTERN.matcher(resourcePathTokens[resourcePathTokens.length - 1]).find();
        }

        return false;
    }

    private boolean isPathPointingToPageVersionsReadme(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 6
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 3]);

            return null != space
                    && StringUtils.equals(AbstractVersionsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 2])
                    && null != page
                    && StringUtils.equals(resourcePathTokens[resourcePathTokens.length - 1], GeneratedResourceReadMeResource.DISPLAY_NAME);
        }

        return false;
    }

    private boolean isPathPointingToPageVersions(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 2]);

            return null != space
                    && StringUtils.equals(AbstractVersionsResource.DISPLAY_NAME, resourcePathTokens[resourcePathTokens.length - 1])
                    && null != page;
        }

        return false;
    }

    private boolean isPathPointingToPageAttachment(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 2]);

            return null != space
                    && !hasAliasInTokens(resourcePathTokens, 3)
                    && null != page
                    && null != attachmentManager.getAttachment(page, resourcePathTokens[resourcePathTokens.length - 1]);
        }

        return false;
    }

    private boolean isPathPointingToPageContent(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 2]);

            return null != space
                    && !hasAliasInTokens(resourcePathTokens, 3)
                    && null != page
                    && StringUtils.equals(page.getTitle() + PageContentResourceImpl.DISPLAY_NAME_SUFFIX, resourcePathTokens[resourcePathTokens.length - 1]);
        }

        return false;
    }

    private boolean isPathPointingToPageUrl(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 5
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);
            Page page = pageManager.getPage(spaceKey, resourcePathTokens[resourcePathTokens.length - 2]);

            return null != space
                    && !hasAliasInTokens(resourcePathTokens, 3)
                    && null != page
                    && StringUtils.equals(page.getTitle() + PageUrlResourceImpl.DISPLAY_NAME_SUFFIX, resourcePathTokens[resourcePathTokens.length - 1]);
        }

        return false;
    }

    private Collection<Page> getUniqueAncestors(List<Page> ancestors) {
        Set<Page> uniqueAncestors = new LinkedHashSet<Page>();

        if (null != ancestors) {
            Set<Long> ancestorIds = new HashSet<Long>();

            for (Page ancestor : ancestors) {
                long ancestorId = ancestor.getId();

                if (!ancestorIds.contains(ancestorId)) {
                    uniqueAncestors.add(ancestor);
                    ancestorIds.add(ancestorId);
                }
            }
        }

        return uniqueAncestors;
    }

    private boolean isPagePathSameAsBreadcrumbs(String[] pathTokensAfterSpace, Page page) {
        if (!webdavSettingsManager.isStrictPageResourcePathCheckingDisabled()) {
            @SuppressWarnings("unchecked")
            List<Page> pageAncestors = page.getAncestors();
            Collection<Page> uniqueAncestors = getUniqueAncestors(pageAncestors);

            LOGGER.debug("Page ancestors: " + pageAncestors);
            LOGGER.debug("Unique ancestors: " + uniqueAncestors);
            LOGGER.debug("Expected ancestor path: " + StringUtils.join(pathTokensAfterSpace, "/"));

            if (pathTokensAfterSpace.length == uniqueAncestors.size()) {
                for (int i = 0; i < pathTokensAfterSpace.length; ++i)
                    if (!StringUtils.equals(pathTokensAfterSpace[i], pageAncestors.get(i).getTitle()))
                        return false;

                return true;
            }

            return false;
        } else {
            LOGGER.debug("Strict page checking disabled. So I'll just assume the paths are ok.");
            return true;
        }
    }

    private boolean isPathPointingToPage(String[] resourcePathTokens) {
        if (resourcePathTokens.length >= 4
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            String pageTitle = resourcePathTokens[resourcePathTokens.length - 1];
            Space space = spaceManager.getSpace(spaceKey);

            LOGGER.debug("Is there a space with the key " + spaceKey + "? " + (null != space));

            if (null != space) {
                Page page = pageManager.getPage(spaceKey, pageTitle);

                LOGGER.debug("Is there a page in space " + spaceKey + " with the title \"" + pageTitle + "\"? " + (null != page));

                return !hasAliasInTokens(resourcePathTokens, 3)
                        && null != page
                        && isPagePathSameAsBreadcrumbs((String[]) ArrayUtils.subarray(resourcePathTokens, 3, resourcePathTokens.length - 1), page);
            }
        }

        return false;
    }

    private boolean isPathPointingToSpaceDescription(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 4
                && (StringUtils.equals(GlobalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1])
                || StringUtils.equals(PersonalSpacesResourceImpl.DISPLAY_NAME, resourcePathTokens[1]))) {
            String spaceKey = resourcePathTokens[2];
            Space space = spaceManager.getSpace(spaceKey);

            if (null != space)
                return StringUtils.equals(space.getName() + SpaceContentResourceImpl.DISPLAY_NAME_SUFFIX, resourcePathTokens[3]);
        }

        return false;
    }

    private boolean isPathPointingToSpace(String[] resourcePathTokens) {
        return resourcePathTokens.length == 3
                && ArrayUtils.indexOf(new String[]{GlobalSpacesResourceImpl.DISPLAY_NAME, PersonalSpacesResourceImpl.DISPLAY_NAME}, resourcePathTokens[1]) >= 0
                && null != spaceManager.getSpace(resourcePathTokens[2]);
    }

    private boolean isPathPointingToEitherGlobalOrPersonalSpaces(String[] resourcePathTokens) {
        return resourcePathTokens.length == 2
                && ArrayUtils.indexOf(new String[]{GlobalSpacesResourceImpl.DISPLAY_NAME, PersonalSpacesResourceImpl.DISPLAY_NAME}, resourcePathTokens[1]) >= 0;
    }

    private boolean isPathPointingToDashboard(String[] resourcePathTokens) {
        return resourcePathTokens.length == 1;
    }

    private boolean isPathPointingSpaceAttachment(String[] resourcePathTokens) {
        if (resourcePathTokens.length == 4 && !StringUtils.equals(resourcePathTokens[resourcePathTokens.length - 1], BlogPostsResourceImpl.DISPLAY_NAME)) {
            Space space = spaceManager.getSpace(resourcePathTokens[2]);
            return null != space
                    && null == pageManager.getPage(space.getKey(), resourcePathTokens[3])
                    && null != attachmentManager.getAttachment(space.getDescription(), resourcePathTokens[3]);
        }

        return false;
    }
}
