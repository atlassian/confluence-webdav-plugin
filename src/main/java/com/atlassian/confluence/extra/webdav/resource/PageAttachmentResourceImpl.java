package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.servlet.download.SafeContentHeaderGuesser;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.user.User;
import com.google.common.base.Throwables;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Represents page attachments.
 *
 * @author weiching.cher
 */
public class PageAttachmentResourceImpl extends AbstractAttachmentResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageAttachmentResourceImpl.class);

    private final PermissionManager permissionManager;

    private final PageManager pageManager;

    private final String spaceKey;

    private final String pageTitle;

    public PageAttachmentResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser,
            @ComponentImport AttachmentManager attachmentManager,
            String spaceKey,
            String pageTitle,
            String attachmentName,
            String userAgent) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, attachmentSafeContentHeaderGuesser, attachmentManager, attachmentName, userAgent);
        this.permissionManager = permissionManager;
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public ContentEntityObject getContentEntityObject() {
        return pageManager.getPage(spaceKey, pageTitle);
    }

    private boolean isDestinationPathValid(String[] resourcePathComponents) {
        /* Only valid if in a page _and not any of_ @news or @versions or @exports */
        for (String resourcePathComponent : resourcePathComponents)
            if (ArrayUtils.indexOf(WebdavConstants.SPECIAL_DIRECTORY_NAMES.toArray(), resourcePathComponent) >= 0)
                return false;

        return resourcePathComponents.length > 4
                && isValidAttachmentName(resourcePathComponents[resourcePathComponents.length - 1]);
    }

    public void copy(DavResource davResource, boolean shallow)
            throws DavException {
        Attachment attachment = getAttachment();
        String[] destinationResourcePathComponents = StringUtils.split(davResource.getResourcePath(), '/');

        if (!isDestinationPathValid(destinationResourcePathComponents))
            throw new DavException(
                    HttpServletResponse.SC_FORBIDDEN,
                    "Cannot move " + getResourcePath() + " to " + StringUtils.join(destinationResourcePathComponents, '/'));

        String destinationSpaceKey = destinationResourcePathComponents[2];
        String destinationPageName = destinationResourcePathComponents[destinationResourcePathComponents.length - 2];
        String destinationAttachmentName = destinationResourcePathComponents[destinationResourcePathComponents.length - 1];
        Page destinationPage = pageManager.getPage(destinationSpaceKey, destinationPageName);

        if (permissionManager.hasCreatePermission(AuthenticatedUserThreadLocal.getUser(),
                destinationPage,
                Attachment.class)) {
            InputStream attachmentInput = null;

            try {
                Attachment attachmentCopy = (Attachment) attachment.clone();

                attachmentCopy.setId(0);
                attachmentCopy.setVersion(1);
                attachmentCopy.setContainer(destinationPage);
                attachmentCopy.setFileName(destinationAttachmentName);

                destinationPage.addAttachment(attachmentCopy);

                attachmentInput = getAttachmentManager().getAttachmentData(attachment);
                getAttachmentManager().saveAttachment(attachmentCopy, null, attachmentInput);
            } catch (IOException e) {
                LOGGER.error("Error copying " + attachment + " to " + StringUtils.join(destinationResourcePathComponents, '/'));
                throw new DavException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            } catch (Exception e) {
                throw Throwables.propagate(e);
            } finally {
                IOUtils.closeQuietly(attachmentInput);
            }
        }
    }

    public void move(DavResource davResource) throws DavException {
        Attachment attachment = getAttachment();
        String[] destinationResourcePathComponents = StringUtils.split(davResource.getResourcePath(), '/');

        User user = AuthenticatedUserThreadLocal.getUser();

        if (davResource instanceof PageContentResourceImpl
                || davResource instanceof SpaceContentResourceImpl
                || davResource instanceof SpaceContentResourceImpl) {
            InputStream attachmentInput = null;
            try {
                attachmentInput = getAttachmentManager().getAttachmentData(attachment);
                davResource.getCollection().addMember(
                        davResource,
                        new PageContentInputContext(attachmentInput)
                );
            } finally {
                IOUtils.closeQuietly(attachmentInput);
                getAttachmentManager().removeAttachmentFromServer(attachment);
            }
        } else {
            if (!isDestinationPathValid(destinationResourcePathComponents))
                throw new DavException(
                        HttpServletResponse.SC_FORBIDDEN,
                        "Cannot move " + getResourcePath() + " to " + StringUtils.join(destinationResourcePathComponents, '/'));

            String destinationSpaceKey = destinationResourcePathComponents[2];
            String destinationPageName = destinationResourcePathComponents[destinationResourcePathComponents.length - 2];
            String destinationAttachmentName = destinationResourcePathComponents[destinationResourcePathComponents.length - 1];
            Page destinationPage = pageManager.getPage(destinationSpaceKey, destinationPageName);

            if (permissionManager.hasCreatePermission(user, destinationPage, Attachment.class)
                    && permissionManager.hasPermission(user, Permission.REMOVE, attachment)) {
                getAttachmentManager().moveAttachment(attachment, destinationAttachmentName, destinationPage);
            } else {
                throw new DavException(HttpServletResponse.SC_FORBIDDEN,
                        "No permission to create attachment in page "
                                + StringUtils.join(destinationResourcePathComponents, '/')
                                + " or no permission to remove "
                                + attachment);
            }
        }
    }

    private static class PageContentInputContext implements InputContext {
        private final InputStream in;

        private final long modificationTime;

        public PageContentInputContext(InputStream in) {
            this.in = in;
            this.modificationTime = System.currentTimeMillis();
        }

        public boolean hasStream() {
            return true;
        }

        public InputStream getInputStream() {
            return in;
        }

        public long getModificationTime() {
            return modificationTime;
        }

        public String getContentLanguage() {
            return null;
        }

        public long getContentLength() {
            return -1;
        }

        public String getContentType() {
            return PageContentResourceImpl.CONTENT_TYPE;
        }

        public String getProperty(String s) {
            return null;
        }
    }
}
