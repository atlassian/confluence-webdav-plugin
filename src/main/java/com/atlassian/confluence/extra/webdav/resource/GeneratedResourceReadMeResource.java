package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;

/**
 * Resource that represents the readme.txt file in <code>&amp;#064;exports</code> and <code>&amp;#064;versions</code>
 */
public class GeneratedResourceReadMeResource extends AbstractTextContentResource {
    public static final String DISPLAY_NAME = "README.txt";

    public GeneratedResourceReadMeResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
    }

    ///CLOVER:OFF
    protected String getReadMeContent() {
        return ConfluenceActionSupport.getTextStatic("webdav.resource.generatedresourcereadme.content");
    }
    ///CLOVER:ON

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return getReadMeContent().getBytes(encoding);
    }

    protected long getCreationtTime() {
        AbstractConfluenceResource parent = (AbstractConfluenceResource) getCollection();
        return parent.getCreationtTime();
    }

    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    private boolean isHidden() {
        DavResource parentResource = getCollection();
        ResourceStates resourceStates = ((ConfluenceDavSession) getSession()).getResourceStates();

        if (parentResource instanceof AbstractVersionsResource)
            return resourceStates.isContentVersionsReadmeHidden(((AbstractVersionsResource) parentResource).getContentEntityObject());
        else if (parentResource instanceof AbstractExportsResource)
            return resourceStates.isContentExportsReadmeHidden(((AbstractExportsResource) parentResource).getContentEntityObject());
        else
            return false; /* Wtf? */
    }

    @Override
    public boolean exists() {
        return super.exists() && !isHidden();
    }
}
