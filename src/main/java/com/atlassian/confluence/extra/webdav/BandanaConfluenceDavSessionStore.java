package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * An implementation of {@link ConfluenceDavSessionStore} backed on Bandana.
 *
 * @since 4.3
 */
@Component
@ExportAsService(ConfluenceDavSessionStore.class)
@ParametersAreNonnullByDefault
public class BandanaConfluenceDavSessionStore implements ConfluenceDavSessionStore {
    private static final Logger logger = LoggerFactory.getLogger(BandanaConfluenceDavSessionStore.class);

    private final BandanaContext bandanaContext = new ConfluenceBandanaContext("com.atlassian.confluence.extra.webdav.sessions");

    private final BandanaManager bandanaManager;
    private final long sessionTimeoutInMillis;

    private static final long DEFAULT_SESSION_TIMEOUT = 1800000;

    /**
     * @param sessionTimeoutInMillis The amount time that must a session must remain inactive before it is elligble for invalidation.
     */
    @VisibleForTesting
    BandanaConfluenceDavSessionStore(BandanaManager bandanaManager, long sessionTimeoutInMillis) {
        this.bandanaManager = bandanaManager;
        this.sessionTimeoutInMillis = sessionTimeoutInMillis;
    }

    @Autowired
    public BandanaConfluenceDavSessionStore(@ComponentImport BandanaManager bandanaManager) {
        this(bandanaManager, DEFAULT_SESSION_TIMEOUT);
    }

    @Override
    public void mapSession(ConfluenceDavSession davSession, String userName) {
        bandanaManager.setValue(bandanaContext, userName, davSession);
    }

    @Override
    @Nullable
    public ConfluenceDavSession getSession(String userName) {
        return (ConfluenceDavSession) bandanaManager.getValue(bandanaContext, userName, false);
    }

    private boolean isSessionExpired(ConfluenceDavSession confluenceDavSession) {
        return System.currentTimeMillis() - confluenceDavSession.getLastActivityTimestamp() > sessionTimeoutInMillis
                && !confluenceDavSession.isCurrentlyBeingUsed();
    }

    @Override
    public void invalidateExpiredSessions() {
        final Set<String> uniqueSessionKeys = getSessionKeys();
        final int sessionSize = uniqueSessionKeys.size();

        if (sessionSize > 100)
            logger.warn("There are " + sessionSize +
                    " active WebDAV sessions just before invalidation. Just thought of telling you that because there seems to be an unusual number of users using the WebDAV plugin.");

        for (String sessionKey : uniqueSessionKeys) {
            final ConfluenceDavSession confluenceDavSession = getSession(sessionKey);

            if (null != confluenceDavSession && isSessionExpired(confluenceDavSession))
                bandanaManager.removeValue(bandanaContext, sessionKey);
        }
    }

    private Set<String> getSessionKeys() {
        final Set<String> keys = new HashSet<>();
        for (String key : bandanaManager.getKeys(bandanaContext)) {
            keys.add(key);
        }
        return keys;
    }

    @Override
    public void executeTaskOnSessions(ConfluenceDavSessionTask confluenceDavSessionTask) {
        Collection<String> uniqueSessionKeys = getSessionKeys();

        for (String sessionKey : uniqueSessionKeys) {
            ConfluenceDavSession confluenceDavSession = getSession(sessionKey);

            confluenceDavSessionTask.execute(confluenceDavSession);
            /* Update the cache with possibly modified session */
            if (confluenceDavSession != null)
                mapSession(confluenceDavSession, confluenceDavSession.getUserName());
        }
    }
}
