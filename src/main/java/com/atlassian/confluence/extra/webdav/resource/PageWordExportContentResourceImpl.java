package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.links.linktypes.PageCreateLink;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkRenderer;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.util.UrlUtil;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the MS Word export of a page in the <code>&amp;#064;exports</code> directory of a page. For instance,
 * if you have a page called &quot;Foo&quot;, you will find &quot;&amp;#064;/foo.doc&quot; in it, and that's the file
 * this class represents.
 * <p>
 * Most of the operations here are copied of from <code>PageWordExportAction</code> in Confluence because it does
 * similar things.
 *
 * @author weiching.cher
 */
public class PageWordExportContentResourceImpl extends AbstractPageExportContentResource {
    public static final String DISPLAY_NAME_SUFFIX = ".doc";

    private static final String CONTENT_TYPE = "application/vnd.ms-word";

    private final ContextPathHolder contextPathHolder;

    private final SettingsManager settingsManager;

    private final XhtmlContent xhtmlContent;

    public PageWordExportContentResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport ContextPathHolder contextPathHolder,
            @ComponentImport SettingsManager settingsManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport XhtmlContent xhtmlContent,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, pageManager, spaceKey, pageTitle);
        this.contextPathHolder = contextPathHolder;
        this.settingsManager = settingsManager;
        this.xhtmlContent = xhtmlContent;
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentWordExportHidden(getPage());
    }

    public void spool(OutputContext outputContext) throws IOException {
        /* These headers are found in PageWordExportAction, so we'll just add them */
        outputContext.setProperty("Cache-Control", "max-age=5");
        outputContext.setProperty("Pragma", StringUtils.EMPTY);
        outputContext.setProperty("Expires", new StringBuffer().append(System.currentTimeMillis() + 300).toString());

        super.spool(outputContext);
    }

    protected String getExportSuffix() {
        return DISPLAY_NAME_SUFFIX;
    }

    protected InputStream getContentInternal() throws IOException {
        /* Mostly copied from PageWordExportAction to generate MS Word exports of a page */
        Page page = getPage();
        PageContext context = page.toPageContext();
        Settings globalSettings = settingsManager.getGlobalSettings();
        Map<String, Object> contextMap;

        context.setBaseUrl(globalSettings.getBaseUrl());
        context.setSiteRoot(contextPathHolder.getContextPath());
        context.setOutputType(RenderContextOutputType.WORD);

        context.setLinkRenderer(new WordDocLinkRenderer(context));

        String renderedContent = "";
        try {
            renderedContent = xhtmlContent.convertStorageToView(page.getBodyContent().getBody(), new DefaultConversionContext(context));
        } catch (XMLStreamException e) {
            throw new IOException(e);
        } catch (XhtmlException e) {
            throw new IOException(e);
        }
        contextMap = new HashMap<String, Object>();
        contextMap.put("renderedContent", renderedContent);
        contextMap.put("domainName", globalSettings.getBaseUrl());
        contextMap.put("page", page);


        return new ByteArrayInputStream(
                VelocityUtils.getRenderedTemplate("templates/extra/webdav/exportword.vm", contextMap).getBytes(globalSettings.getDefaultEncoding()));
    }

    protected String getContentType() {
        return new StringBuffer(CONTENT_TYPE)
                .toString();
    }

    public String getDisplayName() {
        return getPage().getTitle() + DISPLAY_NAME_SUFFIX;
    }

    private static final class WordDocLinkRenderer implements LinkRenderer {
        private final PageContext context;

        public WordDocLinkRenderer(PageContext context) {
            this.context = context;
        }

        public String renderLink(Link link, RenderContext renderContext) {
            StringBuffer buffer = new StringBuffer();

            if (link instanceof UnresolvedLink || link instanceof UnpermittedLink
                    || link instanceof PageCreateLink) {
                buffer.append(link.getLinkBody());
                return buffer.toString();
            }

            buffer.append("<a href=\"");
            if (link.isRelativeUrl())
                buffer.append(context.getBaseUrl());
            buffer.append(link.getUrl());
            buffer.append("\"");

            // decorate the link.
            if (TextUtils.stringSet(link.getTitle())) {
                buffer.append(" title=\"").append(link.getTitle()).append("\"");
            }
            buffer.append(">");
            buffer
                    .append(UrlUtil
                            .escapeUrlFirstCharacter(UrlUtil.escapeSpecialCharacters(link.getLinkBody())));
            buffer.append("</a>");

            return buffer.toString();
        }
    }
}
