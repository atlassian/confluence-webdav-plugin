package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;

/**
 * Represents a page's content. For instance:
 * <code>/Global/ds/myPage/myPage.txt</code>.
 */
public class PageContentResourceImpl extends AbstractTextContentResource {
    public static final String DISPLAY_NAME_SUFFIX = ".txt";

    private final PageManager pageManager;

    private final String spaceKey;

    private final String pageTitle;

    public PageContentResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public Page getPage() {
        return pageManager.getPage(spaceKey, pageTitle);
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentMarkupHidden(getPage());
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return getPage().getBodyContent().getBody().getBytes(encoding);
    }

    public long getModificationTime() {
        return getPage().getLastModificationDate().getTime();
    }

    protected long getCreationtTime() {
        return getPage().getCreationDate().getTime();
    }

    public String getDisplayName() {
        return getPage().getTitle() + DISPLAY_NAME_SUFFIX;
    }
}
