package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

/**
 * Represents a blog post. For instance:
 * <code>/Global/ds/&amp;#064;news/2008/01/01/Blog.txt</code>
 *
 * @author weiching.cher
 */
public class BlogPostContentResourceImpl extends AbstractTextContentResource {
    public static final String DISPLAY_NAME_SUFFIX = ".txt";

    private final UserAccessor userAccessor;

    private final PageManager pageManager;

    private final String spaceKey;

    private final Calendar publishedDate;

    private final String blogTitle;

    private BlogPost blogPost;

    public BlogPostContentResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager,
            @ComponentImport UserAccessor userAccessor,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            int yearPublished,
            int monthPublished,
            int dayPublished,
            String blogTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
        this.userAccessor = userAccessor;
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.blogTitle = blogTitle;
        publishedDate = getBlogPostPublishedDate(yearPublished, monthPublished, dayPublished);
    }

    protected Calendar getBlogPostPublishedDate(
            int yearPublished,
            int monthPublished,
            int dayPublished) {
        Calendar publishedDate = Calendar.getInstance(
                userAccessor.getConfluenceUserPreferences(
                        AuthenticatedUserThreadLocal.getUser()
                ).getTimeZone().getWrappedTimeZone()
        );

        publishedDate.set(yearPublished, monthPublished - 1, dayPublished);
        return publishedDate;
    }

    private BlogPost getBlogPost() {
        if (null == blogPost)
            blogPost = pageManager.getBlogPost(spaceKey, blogTitle, publishedDate);

        return blogPost;
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return getBlogPost().getBodyContent().getBody().getBytes(encoding);
    }

    public long getModificationTime() {
        return getBlogPost().getLastModificationDate().getTime();
    }

    protected long getCreationtTime() {
        return getBlogPost().getCreationDate().getTime();
    }

    public boolean exists() {
        return super.exists() && null != getBlogPost();
    }

    public String getDisplayName() {
        return getBlogPost().getTitle() + DISPLAY_NAME_SUFFIX;
    }
}
