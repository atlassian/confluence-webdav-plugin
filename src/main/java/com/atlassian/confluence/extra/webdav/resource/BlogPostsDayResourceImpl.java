package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

/**
 * Represents the blog posts day directory. For instance:
 * <code>/Global/ds/&amp;#064;news/2008/01/01</code>
 *
 * @author weiching.cher
 */
public class BlogPostsDayResourceImpl extends BlogPostsMonthResourceImpl {
    private static final String DISPLAY_NAME_SUFFIX = ".txt";

    protected final int dayPublished;

    public BlogPostsDayResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport UserAccessor userAccessor,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport PageManager pageManager,
            @ComponentImport SpaceManager spaceManager,
            String spaceKey,
            int yearPublished,
            int monthPublished,
            int dayPublished) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, userAccessor, permissionManager, spaceManager, pageManager, spaceKey, yearPublished, monthPublished);
        this.dayPublished = dayPublished;
    }

    public int getDayPublished() {
        return dayPublished;
    }

    public String getDisplayName() {
        return new DecimalFormat("00").format(dayPublished);
    }

    @Override
    protected Calendar getBlogPostPeriod() {
        Calendar dateWithTimezoneOffset = super.getBlogPostPeriod();

        dateWithTimezoneOffset.set(Calendar.DAY_OF_MONTH, dayPublished);
        return dateWithTimezoneOffset;
    }

    protected DavResourceLocator getBlogPostContentResourceLocator(
            DavResourceLocator locator, StringBuffer childResourcePathBuffer) {
        return locator.getFactory().createResourceLocator(
                locator.getPrefix(),
                locator.getWorkspacePath(),
                childResourcePathBuffer.toString(),
                false);
    }

    public Collection<DavResource> getMemberResources() {
        try {
            DavResourceLocator locator = getLocator();
            String parentPath = getParentResourcePath();
            StringBuffer childResourcePathBuffer = new StringBuffer();
            List<DavResource> members = new ArrayList<DavResource>();
            Calendar blogPostsPublishedDate;
            List<BlogPost> blogPosts;

            blogPostsPublishedDate = getBlogPostPeriod();

            blogPosts = getPermissionManager().getPermittedEntities(
                    AuthenticatedUserThreadLocal.getUser(),
                    Permission.VIEW,
                    getPageManager().getBlogPosts(spaceKey, blogPostsPublishedDate, Calendar.DATE)
            );

            for (BlogPost blogPost : blogPosts) {
                DavResourceLocator blogPostContentResourceLocator;

                childResourcePathBuffer.setLength(0);
                childResourcePathBuffer.append(parentPath)
                        .append('/').append(blogPost.getPostingDayOfMonth())
                        .append('/').append(blogPost.getTitle()).append(DISPLAY_NAME_SUFFIX);

                blogPostContentResourceLocator =
                        getBlogPostContentResourceLocator(locator,
                                childResourcePathBuffer);

                members.add(getFactory().createResource(blogPostContentResourceLocator, getSession()));
            }

            return members;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
