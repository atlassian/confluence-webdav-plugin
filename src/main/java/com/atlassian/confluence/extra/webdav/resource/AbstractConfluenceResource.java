package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.server.io.IOUtil;
import org.apache.jackrabbit.util.Text;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.apache.jackrabbit.webdav.lock.ActiveLock;
import org.apache.jackrabbit.webdav.lock.LockEntry;
import org.apache.jackrabbit.webdav.lock.LockInfo;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.Scope;
import org.apache.jackrabbit.webdav.lock.SupportedLock;
import org.apache.jackrabbit.webdav.lock.Type;
import org.apache.jackrabbit.webdav.property.DavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyIterator;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertyNameIterator;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.DefaultDavProperty;
import org.apache.jackrabbit.webdav.property.HrefProperty;
import org.apache.jackrabbit.webdav.property.ResourceType;
import org.apache.jackrabbit.webdav.simple.DavResourceImpl;
import org.apache.jackrabbit.webdav.version.DeltaVConstants;
import org.apache.jackrabbit.webdav.version.SupportedMethodSetProperty;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static java.lang.String.format;

/**
 * Abstract parent class for every type of resource shown by WebDAV clients.
 *
 * @author weiching.cher
 */
public abstract class AbstractConfluenceResource implements DavResource {
    private final DavResourceLocator davResourceLocator;

    private final DavResourceFactory davResourceFactory;

    private LockManager lockManager;

    private final ConfluenceDavSession davSession;

    private DavPropertySet propertySet;

    public AbstractConfluenceResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession) {
        this.davResourceLocator = davResourceLocator;
        this.davResourceFactory = davResourceFactory;
        this.lockManager = lockManager;
        this.davSession = davSession;
    }

    protected abstract long getCreationtTime();

    protected abstract SupportedLock getSupportedLock();

    /**
     * {@inheritDoc}
     *
     * @return Returns <code>true</code> if and only if this the parent of this resource exists.
     */
    public boolean exists() {
        DavResource parent = getCollection();
        return null != parent && parent.exists();
    }

    protected String getParentResourcePath() {
        String[] resourcePathTokens = StringUtils.split(getResourcePath(), '/');
        if (resourcePathTokens == null)
            resourcePathTokens = new String[0];

        StringBuilder parentPathBuffer = new StringBuilder();

        for (int i = 0; i < resourcePathTokens.length - 1; ++i)
            parentPathBuffer.append('/').append(resourcePathTokens[i]);

        return parentPathBuffer.toString();
    }

    protected String getWorkspaceHref() {
        return getLocator().getWorkspacePath();
    }

    public DavResource getCollection() {
        try {
            DavResourceLocator locator = getLocator();
            DavResourceLocator parentLocator = locator.getFactory().createResourceLocator(Text.escape(locator.getPrefix()), Text.escape(getParentResourcePath()));

            return getFactory().createResource(parentLocator, getSession());
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }


    /**
     * Returns the WebDAV compliance class of this resource.
     *
     * @return Always returns {@link DavResourceImpl#COMPLIANCE_CLASSES}
     */
    public String getComplianceClass() {
        return DavResourceImpl.COMPLIANCE_CLASSES;
    }


    /**
     * Gets a comma separated list of supported HTTP methods.
     *
     * @return Always returns {@link DavResource#METHODS}
     */
    public String getSupportedMethods() {
        return DavResource.METHODS;
    }

    public DavResourceLocator getLocator() {
        return davResourceLocator;
    }

    public String getResourcePath() {
        return getLocator().getResourcePath();
    }

    public String getHref() {
        return getLocator().getHref(isCollection());
    }


    /**
     * Returns the last modified time of this resource.
     *
     * @return Always returns the current system time in milli seconds.
     */
    public long getModificationTime() {
        return System.currentTimeMillis(); /* Always modified */
    }

    public DavPropertyName[] getPropertyNames() {
        return getProperties().getPropertyNames();
    }

    public DavProperty getProperty(DavPropertyName davPropertyName) {
        return getProperties().get(davPropertyName);
    }

    public DavPropertySet getProperties() {
        if (null == propertySet) {
            propertySet = new DavPropertySet();
            initProperties(propertySet);
        }

        return propertySet;
    }


    protected void initProperties(final DavPropertySet propertySet) {
        if (getDisplayName() != null) {
            propertySet.add(new DefaultDavProperty(DavPropertyName.DISPLAYNAME, getDisplayName()));
        }
        if (isCollection()) {
            propertySet.add(new ResourceType(ResourceType.COLLECTION));
            // Windows XP support
            propertySet.add(new DefaultDavProperty(DavPropertyName.ISCOLLECTION, "1"));
        } else {
            propertySet.add(new ResourceType(ResourceType.DEFAULT_RESOURCE));
            // Windows XP support
            propertySet.add(new DefaultDavProperty(DavPropertyName.ISCOLLECTION, "0"));
        }

        // default last modified
        String lastModified = IOUtil.getLastModified(getModificationTime());
        propertySet.add(new DefaultDavProperty(DavPropertyName.GETLASTMODIFIED, lastModified));

        // default creation time
        propertySet.add(
                new DefaultDavProperty(
                        DavPropertyName.CREATIONDATE,
                        DavConstants.creationDateFormat.format(
                                new Date(getCreationtTime()))));

        propertySet.add(new SupportedMethodSetProperty(getSupportedMethods().split(",\\s")));

        // 'workspace' property as defined by RFC 3253
        String workspaceHref = getWorkspaceHref();
        if (workspaceHref != null) {
            propertySet.add(new HrefProperty(DeltaVConstants.WORKSPACE, workspaceHref, true));
        }
    }


    /**
     * This method will throw a {@link org.apache.jackrabbit.webdav.DavException} with
     * {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN} as the error code every time.
     */
    public void setProperty(DavProperty davProperty) throws DavException {
        throw new DavException(DavServletResponse.SC_FORBIDDEN);
    }

    /**
     * This method will throw a {@link org.apache.jackrabbit.webdav.DavException} with
     * {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN} as the error code every time.
     */
    public void removeProperty(DavPropertyName davPropertyName) throws DavException {
        throw new DavException(DavServletResponse.SC_FORBIDDEN);
    }

    /**
     * Converts call to {@link #alterProperties(java.util.List)}.
     */
    public MultiStatusResponse alterProperties(DavPropertySet davPropertySet, DavPropertyNameSet davPropertyNameSet) throws DavException {
        List changeList = new ArrayList();

        if (null != davPropertySet) {
            for (DavPropertyIterator davPropertyIterator = davPropertySet.iterator(); davPropertyIterator.hasNext(); )
                changeList.add(davPropertyIterator.nextProperty());
        }

        if (null != davPropertyNameSet) {
            for (DavPropertyNameIterator davPropertyNameIterator = davPropertyNameSet.iterator(); davPropertyNameIterator.hasNext(); )
                changeList.add(davPropertyNameIterator.nextPropertyName());
        }

        return alterProperties(changeList);
    }


    /**
     * Always return a {@link org.apache.jackrabbit.webdav.MultiStatusResponse} with entries having status code
     * {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     */
    public MultiStatusResponse alterProperties(List changeList) throws DavException {
        MultiStatusResponse msr = new MultiStatusResponse(getHref(), null);

        Iterator it = changeList.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            int statusCode = DavServletResponse.SC_FORBIDDEN;

            if (o instanceof DavProperty) {
                msr.add(((DavProperty) o).getName(), statusCode);
            } else {
                msr.add((DavPropertyName) o, statusCode);
            }
        }

        return msr;
    }


    /**
     * Checks if this resource allows the specified type of lock in the specified scope.
     *
     * @return Returns <code>true</code> if the lock type in the specified scope is supported, <code>false</code> otherwise.
     */
    public boolean isLockable(Type type, Scope scope) {
        SupportedLock supportedLock = getSupportedLock();
        return null != supportedLock && supportedLock.isSupportedLock(type, scope);
    }


    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException Thrown if this resource couldn't find a {@link org.apache.jackrabbit.webdav.lock.LockManager} to use to handle the lock operation.
     */
    public boolean hasLock(Type type, Scope scope) {
        if (null == lockManager) {
            throw new NullPointerException(format("Unable to check if %s is locked because LockManager is null.", getResourcePath()));
        } else {
            return null != getLock(type, scope);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @throws NullPointerException Thrown if this resource couldn't find a {@link org.apache.jackrabbit.webdav.lock.LockManager} to use to handle the lock operation.
     */
    public ActiveLock getLock(Type type, Scope scope) {
        if (null == lockManager)
            throw new NullPointerException(format("Unable to query for lock on %s because LockManager is null.", getResourcePath()));
        else
            return lockManager.getLock(type, scope, this);
    }

    /**
     * Gets all the active locks on this resource.
     *
     * @return An array of {@link org.apache.jackrabbit.webdav.lock.ActiveLock} instances representing the locks on this
     * resource.
     */
    public ActiveLock[] getLocks() {
        SupportedLock supportedLock = getSupportedLock();

        if (null != supportedLock) {
            List<ActiveLock> activeLocks = new ArrayList<ActiveLock>();

            for (final Iterator i = supportedLock.getSupportedLocks(); i.hasNext(); ) {
                LockEntry lockEntry = (LockEntry) i.next();
                ActiveLock activeLock = getLock(lockEntry.getType(), lockEntry.getScope());

                if (null != activeLock)
                    activeLocks.add(activeLock);
            }


            return activeLocks.toArray(new ActiveLock[activeLocks.size()]);
        }

        return new ActiveLock[0];
    }


    /**
     * {@inheritDoc}
     *
     * @throws DavException Thrown if this resource couldn't find a {@link org.apache.jackrabbit.webdav.lock.LockManager} to use to handle the lock operation.
     */
    public ActiveLock lock(LockInfo lockInfo) throws DavException {
        if (null == lockManager)
            throw new DavException(HttpServletResponse.SC_PRECONDITION_FAILED,
                    format("Unable to lock on %s because LockManager is null.", getResourcePath()));
        else
            return lockManager.createLock(lockInfo, this);
    }


    /**
     * {@inheritDoc}
     *
     * @throws DavException Thrown if this resource couldn't find a {@link org.apache.jackrabbit.webdav.lock.LockManager} to use to handle the lock operation.
     */
    public ActiveLock refreshLock(LockInfo lockInfo, String lockToken) throws DavException {
        if (null == lockManager)
            throw new DavException(HttpServletResponse.SC_PRECONDITION_FAILED,
                    format("Unable to refresh lock on %s because LockManager is null.", getResourcePath()));
        else
            return lockManager.refreshLock(lockInfo, lockToken, this);
    }


    /**
     * {@inheritDoc}
     *
     * @throws DavException Thrown if this resource couldn't find a {@link org.apache.jackrabbit.webdav.lock.LockManager} to use to handle the lock operation.
     */
    public void unlock(String lockToken) throws DavException {
        if (null == lockManager)
            throw new DavException(HttpServletResponse.SC_PRECONDITION_FAILED,
                    format("Unable to release lock on %s because LockManager is null.", getResourcePath()));
        else
            lockManager.releaseLock(lockToken, this);
    }

    public void addLockManager(LockManager lockManager) {
        this.lockManager = lockManager;
    }

    public DavResourceFactory getFactory() {
        return davResourceFactory;
    }

    public DavSession getSession() {
        return davSession;
    }


    /**
     * At this level, a {@link org.apache.jackrabbit.webdav.DavException} will be raised with the error
     * code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     *
     * @throws DavException Thrown with the error code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     */
    public void addMember(DavResource davResource, InputContext inputContext) throws DavException {
        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Not allowed to add members to " + getHref());
    }

    /**
     * At this level, a {@link org.apache.jackrabbit.webdav.DavException} will be raised with the error
     * code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     *
     * @throws DavException Thrown with the error code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     */
    public void removeMember(DavResource davResource) throws DavException {
        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Not allowed to remove members from " + getHref());
    }

    /**
     * At this level, a {@link org.apache.jackrabbit.webdav.DavException} will be raised with the error
     * code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     *
     * @throws DavException Thrown with the error code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     */
    public void move(DavResource davResource) throws DavException {
        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Not allowed to move " + getHref());
    }

    /**
     * At this level, a {@link org.apache.jackrabbit.webdav.DavException} will be raised with the error
     * code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     *
     * @throws DavException Thrown with the error code of {@link javax.servlet.http.HttpServletResponse#SC_FORBIDDEN}.
     */
    public void copy(DavResource davResource, boolean shallow) throws DavException {
        throw new DavException(HttpServletResponse.SC_FORBIDDEN, "Not allowed to copy from " + getHref());
    }
}
