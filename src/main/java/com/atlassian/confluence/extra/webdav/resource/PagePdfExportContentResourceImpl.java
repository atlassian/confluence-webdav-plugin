package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.flyingpdf.PdfExporterService;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.importexport.ImportExportException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Represents the PDF export of page in a page's <code>&amp;#064;exports</code> directory.
 *
 * @author weiching.cher
 */
public class PagePdfExportContentResourceImpl extends AbstractPageExportContentResource {

    public static final String CONTENT_TYPE = "application/pdf";
    public static final String DISPLAY_NAME_SUFFIX = ".pdf";
    private static final Logger LOGGER = LoggerFactory.getLogger(PagePdfExportContentResourceImpl.class);

    private final PdfExporterService pdfExporterService;

    public PagePdfExportContentResourceImpl(
            final DavResourceLocator davResourceLocator,
            final DavResourceFactory davResourceFactory,
            final LockManager lockManager,
            final ConfluenceDavSession davSession,
            @ComponentImport final PageManager pageManager,
            @ComponentImport final PdfExporterService pdfExporterService,
            final String spaceKey,
            final String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, pageManager, spaceKey, pageTitle);
        this.pdfExporterService = pdfExporterService;
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentPdfExportHidden(getPage());
    }

    protected String getExportSuffix() {
        return DISPLAY_NAME_SUFFIX;
    }

    private File generatePdfExportFromPdfExporterService() throws ImportExportException {
        return pdfExporterService.createPdfForPage(
                AuthenticatedUserThreadLocal.get(),
                getPage(),
                ServletActionContext.getRequest().getContextPath());
    }

    protected InputStream getContentInternal() {
        try {
            return new BufferedInputStream(
                    Files.newInputStream(generatePdfExportFromPdfExporterService().toPath())
            );
        } catch (Exception e) {
            LOGGER.error("Error exporting " + getPage() + " as PDF. Returning InputStream with one byte", e);
            return new ByteArrayInputStream(new byte[1]);
        }
    }

    protected String getContentType() {
        return CONTENT_TYPE;
    }

    public String getDisplayName() {
        return getPage().getTitle() + DISPLAY_NAME_SUFFIX;
    }
}
