package com.atlassian.confluence.extra.webdav;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

/**
 * Class that represents the supported configuration by the plugin.
 * This class <em>is not</em> thread-safe. It is recommend that you do not share the same instance with multiple
 * threads.
 */
public class WebdavSettings implements Serializable {
    /**
     * Set of {@link String} which represent regular expressions of user agents which will be denied of WebDAV writes.
     */
    private Set<String> excludedClientUserAgentRegexes;

    /**
     * Flag indicating whether the <code>@exports</code> directory is visible to WebDAV clients.
     */
    private boolean contentExportsResourceEnabled;

    /**
     * Flag indicating whether the <code>@versions</code> directory is visible to WebDAV clients.
     */
    private boolean contentVersionsResourceEnabled;


    /**
     * Flag indicating whether the <code>&lt;page title&gt;.url</code> file is visible to WebDAV clients.
     */
    private boolean contentUrlResourceEnabled;

    /**
     * Flag that indicates if strict path checking when locating page resources in
     * {@link com.atlassian.confluence.extra.webdav.ConfluenceResourceFactory} should
     * be disabled.
     */
    private boolean strictPageResourcePathCheckingDisabled;

    public WebdavSettings() {
        this(null);
    }

    public WebdavSettings(WebdavSettings webdavSettings) {
        excludedClientUserAgentRegexes = new LinkedHashSet<>();
        setContentExportsResourceEnabled(true);
        setContentVersionsResourceEnabled(true);
        setContentUrlResourceEnabled(true);
        setStrictPageResourcePathCheckingDisabled(false);

        if (null != webdavSettings) {
            setExcludedClientUserAgentRegexes(webdavSettings.getExcludedClientUserAgentRegexes());
            setContentExportsResourceEnabled(webdavSettings.isContentExportsResourceEnabled());
            setContentVersionsResourceEnabled(webdavSettings.isContentVersionsResourceEnabled());
            setContentUrlResourceEnabled(webdavSettings.isContentUrlResourceEnabled());
            setStrictPageResourcePathCheckingDisabled(webdavSettings.isStrictPageResourcePathCheckingDisabled());
        }
    }

    public Set<String> getExcludedClientUserAgentRegexes() {
        return unmodifiableSet(excludedClientUserAgentRegexes);
    }

    public void setExcludedClientUserAgentRegexes(Collection<String> regexes) {
        excludedClientUserAgentRegexes.clear();
        excludedClientUserAgentRegexes.addAll(regexes);
    }

    public boolean isContentExportsResourceEnabled() {
        return contentExportsResourceEnabled;
    }

    public void setContentExportsResourceEnabled(boolean exportsDirectoryEnabled) {
        this.contentExportsResourceEnabled = exportsDirectoryEnabled;
    }

    public boolean isContentVersionsResourceEnabled() {
        return contentVersionsResourceEnabled;
    }

    public void setContentVersionsResourceEnabled(boolean contentVersionsResourceEnabled) {
        this.contentVersionsResourceEnabled = contentVersionsResourceEnabled;
    }

    public boolean isContentUrlResourceEnabled() {
        return contentUrlResourceEnabled;
    }

    public void setContentUrlResourceEnabled(boolean contentUrlResourceEnabled) {
        this.contentUrlResourceEnabled = contentUrlResourceEnabled;
    }

    public boolean isStrictPageResourcePathCheckingDisabled() {
        return strictPageResourcePathCheckingDisabled;
    }

    public void setStrictPageResourcePathCheckingDisabled(boolean strictPageResourcePathCheckingDisabled) {
        this.strictPageResourcePathCheckingDisabled = strictPageResourcePathCheckingDisabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WebdavSettings that = (WebdavSettings) o;

        if (contentExportsResourceEnabled != that.contentExportsResourceEnabled) return false;
        if (contentUrlResourceEnabled != that.contentUrlResourceEnabled) return false;
        if (contentVersionsResourceEnabled != that.contentVersionsResourceEnabled) return false;
        if (strictPageResourcePathCheckingDisabled != that.strictPageResourcePathCheckingDisabled) return false;
        return Objects.equals(excludedClientUserAgentRegexes, that.excludedClientUserAgentRegexes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                excludedClientUserAgentRegexes,
                contentExportsResourceEnabled,
                contentVersionsResourceEnabled,
                contentUrlResourceEnabled,
                strictPageResourcePathCheckingDisabled
        );
    }
}
