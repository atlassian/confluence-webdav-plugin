package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.servlet.download.SafeContentHeaderGuesser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

/**
 * An abstract representation of an attachment.
 *
 * @author weiching.cher
 */
public abstract class AbstractAttachmentResource extends AbstractContentResource {

    private final AttachmentManager attachmentManager;
    private final SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser;

    private final String attachmentName;
    private final String userAgent;

    private Attachment attachment;

    public AbstractAttachmentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SafeContentHeaderGuesser attachmentSafeContentHeaderGuesser,
            @ComponentImport AttachmentManager attachmentManager,
            String attachmentName,
            String userAgent) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.attachmentSafeContentHeaderGuesser = attachmentSafeContentHeaderGuesser;
        this.attachmentManager = attachmentManager;
        this.attachmentName = attachmentName;
        this.userAgent = userAgent;
    }

    protected AttachmentManager getAttachmentManager() {
        return attachmentManager;
    }

    /**
     * Checks if the attachment name is valid for Confluence. This mimics the file name validation done
     * by {@link com.atlassian.confluence.pages.DefaultFileUploadManager}.
     *
     * @param resourceName The attachment file name
     * @return <code>true</code> if the name is valid, <code>false</code> otherwise.
     */
    public static boolean isValidAttachmentName(String resourceName) {
        return !resourceName.contains("&")
                && !resourceName.contains("+")
                && !resourceName.contains("?")
                && !resourceName.contains("|")
                && !resourceName.contains("=");
    }

    public abstract ContentEntityObject getContentEntityObject();

    public boolean exists() {
        return super.exists()
                && null != getAttachment()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isAttachmentHidden(getAttachment());
    }

    public Attachment getAttachment() {
        if (null == attachment)
            attachment = attachmentManager.getAttachment(getContentEntityObject(), attachmentName);

        return attachment;
    }

    public long getModificationTime() {
        return getAttachment().getLastModificationDate().getTime();
    }

    public InputStream getContent() {
        return attachmentManager.getAttachmentData(getAttachment());
    }

    protected long getContentLength() {
        return getAttachment().getFileSize();
    }

    protected String getContentType() {
        final Map<String, String> contentTypeMap = getHeaders();
        return contentTypeMap.get("Content-Type");
    }

    protected Map<String, String> getHeaders() {
        try {
            return attachmentSafeContentHeaderGuesser.computeAttachmentHeaders(
                    getAttachment().getMediaType(),
                    new BufferedInputStream(attachmentManager.getAttachmentData(attachment)),
                    getAttachment().getFileName(),
                    userAgent,
                    getContentLength(),
                    false,
                    Collections.emptyMap()

            );
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    protected long getCreationtTime() {
        return getAttachment().getCreationDate().getTime();
    }

    public String getDisplayName() {
        return getAttachment().getFileName();
    }
}
