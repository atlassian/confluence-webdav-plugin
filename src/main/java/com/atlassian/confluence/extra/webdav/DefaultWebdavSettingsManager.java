package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.setup.xstream.ConfluenceXStreamManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Persists and retrieves {@link com.atlassian.confluence.extra.webdav.WebdavSettings} to/via Bandana.
 *
 * @deprecated since 4.3. Use {@link BandanaWebdavSettingsManager}. Caching of bandana is now handled by the
 * Confluence host, noi need to do it here.
 */
@Deprecated
public class DefaultWebdavSettingsManager extends BandanaWebdavSettingsManager {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultWebdavSettingsManager.class);

    private static final String CACHE_KEY = "com.atlassian.confluence.extra.webdav.settings";

    private final CacheManager cacheManager;

    public DefaultWebdavSettingsManager(@ComponentImport BandanaManager bandanaManager,
                                        @ComponentImport CacheManager cacheManager,
                                        @ComponentImport ConfluenceXStreamManager xStreamManager) {
        super(bandanaManager, xStreamManager);
        this.cacheManager = cacheManager;
    }

    private String getCacheEntryKey() {
        return CACHE_KEY + ".global"; /* More logical, and just maybe we can expand on this to support personalized
        settings */
    }

    private WebdavSettings getCachedSetings() {
        Cache webdavSettingsCache = cacheManager.getCache(CACHE_KEY);
        String cacheEntryKey = getCacheEntryKey();
        try {
            return (WebdavSettings) webdavSettingsCache.get(cacheEntryKey);
        } catch (ClassCastException cce) {
            // If this happens, either someone stored something that is not a WebdavSettings into the cache with the same key
            // or the plugin was upgraded and the WebdavSettings loaded by the previous class loader can't be cast to
            // a WebdavSettings.
            LOG.warn("Unable to cast the cached WebdavSettings retrieved with key " + cacheEntryKey + " to a WebdavSettings. It will be purged from the cache.", cce);
            webdavSettingsCache.remove(cacheEntryKey);
        }

        return null;
    }

    private void cacheSettings(WebdavSettings webdavSettings) {
        Cache webdavSettingsCache = cacheManager.getCache(CACHE_KEY);
        webdavSettingsCache.put(getCacheEntryKey(), webdavSettings);
    }

    public void save(WebdavSettings webdavSettings) {
        super.save(webdavSettings);
        cacheSettings(new WebdavSettings(webdavSettings));
    }

    public WebdavSettings getWebdavSettings() {
        WebdavSettings webdavSettings = getCachedSetings();

        if (null != webdavSettings)
            return new WebdavSettings(webdavSettings); /* Because WebdavSettings is not immutable */

        webdavSettings = super.getWebdavSettings();

        cacheSettings(new WebdavSettings(webdavSettings));

        return webdavSettings;
    }
}
