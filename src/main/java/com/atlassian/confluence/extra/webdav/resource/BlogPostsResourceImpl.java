package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents the <code>&amp;#064news</code> directory. For instance:
 * <code>/Global/ds/&amp;#064;news</code>.
 *
 * @author weiching.cher
 */
public class BlogPostsResourceImpl extends AbstractCollectionResource {
    public static final String DISPLAY_NAME = "@news";

    private final PermissionManager permissionManager;

    private final SpaceManager spaceManager;

    private final PageManager pageManager;

    protected final String spaceKey;

    private Space space;

    public BlogPostsResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            String spaceKey) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.permissionManager = permissionManager;
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
    }

    protected PermissionManager getPermissionManager() {
        return permissionManager;
    }

    protected SpaceManager getSpaceManager() {
        return spaceManager;
    }

    protected PageManager getPageManager() {
        return pageManager;
    }

    public Space getSpace() {
        if (null == space)
            space = spaceManager.getSpace(spaceKey);

        return space;
    }

    protected long getCreationtTime() {
        return getSpace().getCreationDate().getTime();
    }

    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    public Collection<DavResource> getMemberResources() {
        try {
            List<DavResource> members = new ArrayList<DavResource>();
            DavResourceLocator locator = getLocator();
            String parentPath = getParentResourcePath();
            StringBuffer childResourcePathBuffer = new StringBuffer();
            Set<String> uniqueChildPaths = new HashSet<String>();
            @SuppressWarnings("unchecked")
            List<BlogPost> blogPosts = permissionManager.getPermittedEntities(
                    AuthenticatedUserThreadLocal.getUser(),
                    Permission.VIEW,
                    pageManager.getBlogPosts(getSpace(), true)
            );

            for (BlogPost blogPost : blogPosts) {
                childResourcePathBuffer.setLength(0);
                childResourcePathBuffer.append(parentPath)
                        .append('/').append(DISPLAY_NAME)
                        .append('/').append(blogPost.getPostingYear());

                uniqueChildPaths.add(childResourcePathBuffer.toString());
            }

            for (String uniqueChildPage : uniqueChildPaths) {
                DavResourceLocator blogPostYearResourceLocator;

                blogPostYearResourceLocator = locator.getFactory().createResourceLocator(
                        locator.getPrefix(), locator.getWorkspacePath(), uniqueChildPage, false);

                members.add(getFactory().createResource(blogPostYearResourceLocator, getSession()));
            }

            return members;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
