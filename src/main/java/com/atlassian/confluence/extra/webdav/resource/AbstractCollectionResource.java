package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.util.PlainTextToHtmlConverter;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceIteratorImpl;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.SupportedLock;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Abstract parent class for all directories.
 *
 * @author weiching.cher
 */
public abstract class AbstractCollectionResource extends AbstractConfluenceResource {
    protected static final String TEXTEDIT_TEMP_FOLDER_NAME = "(A Document Being Saved By TextEdit)";

    public AbstractCollectionResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
    }

    protected SupportedLock getSupportedLock() {
        return new SupportedLock();
    }

    /**
     * Checks if this resource is a directory.
     *
     * @return Always returns <code>true</code>.
     */
    public final boolean isCollection() {
        return true;
    }

    /**
     * @return whether the collection resources this resource returns have already been validated as existing for the
     * currently authenticated user.
     */
    public boolean isValidated() {
        return false;
    }

    /**
     * Generates a HTML view of the structure of this resource. The generated HTML will show
     * this resource, its parent and its children.
     *
     * @param outputContext The placeholder where the output will be written to.
     */
    public void spool(OutputContext outputContext) throws IOException {
        if (outputContext.hasStream()) {
            /* Show a listing of children in HTML format */
            OutputStream outputStream = outputContext.getOutputStream();
            StringBuffer stringBuffer = new StringBuffer();

            stringBuffer
                    .append("<html>")
                    .append("<head><title>").append(PlainTextToHtmlConverter.encodeHtmlEntities(getDisplayName())).append("</title></head>")
                    .append("<body>");

            try {
                DavResource parentResource = getCollection();
                DavResourceIterator davResourceIterator = getMembers();
                byte[] htmlBytes;

                if (null != davResourceIterator || null != parentResource) {
                    stringBuffer.append("<ul>");

                    if (null != getCollection())
                        stringBuffer.append("<li><a href=\"").append(parentResource.getHref()).append("\">../</a></li>");

                    if (null != davResourceIterator) {
                        while (davResourceIterator.hasNext()) {
                            DavResource children = davResourceIterator.nextResource();

                            stringBuffer.append("<li><a href=\"")
                                    .append(children.getHref())
                                    .append("\">").append(PlainTextToHtmlConverter.encodeHtmlEntities(children.getDisplayName())).append("</a></li>");
                        }
                    }

                    stringBuffer.append("</ul>");
                }


                stringBuffer.append("</body></html>");

                htmlBytes = stringBuffer.toString().getBytes("UTF-8");

                outputContext.setContentLength(htmlBytes.length);
                outputContext.setContentType("text/html");
                outputContext.setModificationTime(getModificationTime());

                outputStream.write(htmlBytes);
                outputStream.flush();
            } finally {
                IOUtils.closeQuietly(outputStream);
            }
        } else {
            outputContext.setContentLength(0);
            outputContext.setModificationTime(getModificationTime());
        }
    }

    public boolean isTextEditCreatingTempFolder(String resourceName, ConfluenceDavSession confluenceDavSession) {
        return confluenceDavSession.isClientFinder() && resourceName.contentEquals(TEXTEDIT_TEMP_FOLDER_NAME);
    }

    protected abstract Collection<DavResource> getMemberResources();

    public final DavResourceIterator getMembers() {
        List<DavResource> membersResources = new ArrayList<DavResource>();
        final boolean isValidated = this.isValidated();

        for (DavResource davResource : getMemberResources()) {
            if (isValidated || davResource.exists())
                membersResources.add(davResource);
        }

        return new DavResourceIteratorImpl(membersResources);
    }
}
