package com.atlassian.confluence.extra.webdav;

import com.atlassian.plugin.ModuleDescriptor;
import org.apache.jackrabbit.webdav.DavResourceFactory;

/**
 * A module descriptor for {@link DavResourceFactory} objects.
 */
public interface DavResourceFactoryModuleDescriptor extends ModuleDescriptor<DavResourceFactory> {
    DavResourceFactory getModule();

    /**
     * The first section of the webdav path, this identifies which ResourceFactory to use.
     *
     * @return the workspace name, with no / before or after.
     */
    String getWorkspaceName();
}
