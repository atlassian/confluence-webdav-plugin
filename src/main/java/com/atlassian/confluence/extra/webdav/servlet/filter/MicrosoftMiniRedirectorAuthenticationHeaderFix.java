package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.util.UserAgentUtil;
import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * When the plugin is mounted as a network drive (via mod_proxy or similar things), Windows send the username in the format
 * of <code>&lt;hostname&gt;/&lt;username&gt;</code>. Therefore, this filter is a fix (or hack) that strips the host name
 * before passing it to the processing servlet.
 */
public class MicrosoftMiniRedirectorAuthenticationHeaderFix extends AbstractHttpFilter {
    private HttpServletRequest rewriteDestinationHeader(HttpServletRequest httpServletRequest) throws IOException, ServletException {
        String authorizationHeader = httpServletRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION);

        if (StringUtils.isNotBlank(authorizationHeader)) {
            String[] authTokens = StringUtils.split(StringUtils.trim(authorizationHeader), ' ');

            if (authTokens.length != 2)
                throw new ServletException("Malformed Authorization header: " + StringUtils.join(authTokens));

            String credentials = new String(Base64.decodeBase64(authTokens[1].getBytes("UTF-8")), "UTF-8");
            String[] credentialTokens = StringUtils.split(credentials, ':');

            if (credentialTokens.length == 2) {
                int indexOfBackSlash = credentialTokens[0].indexOf('\\');
                if (indexOfBackSlash >= 0 && indexOfBackSlash < credentialTokens[0].length() - 1) {
                    credentialTokens[0] = credentialTokens[0].substring(indexOfBackSlash + 1);

                    /* Rewrite the value of the header */
                    credentials = new String(
                            Base64.encodeBase64(StringUtils.join(credentialTokens, ':').getBytes("UTF-8")),
                            "UTF-8"
                    );

                    return new HttpServletRequestWrapperWithModifiedAuthorizationHeader(
                            httpServletRequest,
                            StringUtils.join(
                                    new String[]{
                                            authTokens[0],
                                            credentials
                                    }, ' '
                            )
                    );
                }
            }
        }

        return httpServletRequest;
    }

    protected boolean handles(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return UserAgentUtil.isMicrosoftMiniRedirector(request.getHeader(WebdavConstants.HEADER_USER_AGENT));
    }

    public void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(rewriteDestinationHeader(httpServletRequest), httpServletResponse);
    }

    private static class HttpServletRequestWrapperWithModifiedAuthorizationHeader extends HttpServletRequestWrapper {
        private final String authorizationHeaderValue;

        public HttpServletRequestWrapperWithModifiedAuthorizationHeader(
                HttpServletRequest httpServletRequest,
                String authorizationHeaderValue) {
            super(httpServletRequest);
            this.authorizationHeaderValue = authorizationHeaderValue;
        }

        public String getHeader(String name) {
            if (StringUtils.equals(WebdavConstants.HEADER_AUTHORIZATION, name))
                return authorizationHeaderValue;
            else
                return super.getHeader(name);
        }
    }
}
