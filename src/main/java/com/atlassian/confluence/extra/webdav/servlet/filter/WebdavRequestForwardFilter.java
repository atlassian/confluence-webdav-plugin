package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.servlet.ConfluenceWebdavServlet;
import com.atlassian.confluence.extra.webdav.util.UserAgentUtil;
import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter which forwards requests coming from /webdav/* to /plugins/servlet/confluence/default, by default
 * {@link ConfluenceWebdavServlet} can handle the forwarded request.
 */
public class WebdavRequestForwardFilter extends AbstractPrefixAwareFilter {
    private static final Logger logger = LoggerFactory.getLogger(WebdavRequestForwardFilter.class);

    private String mountPointPrefix;

    private FilterConfig filterConfig;

    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        this.filterConfig = filterConfig;
        mountPointPrefix = StringUtils.defaultString(filterConfig.getInitParameter("mount-point-prefix"));
    }

    protected boolean handles(HttpServletRequest request, HttpServletResponse response) {
        String userAgent = request.getHeader(WebdavConstants.HEADER_USER_AGENT);
        /* Only handle empty context root because Windows can only map a WebDAV location to a drive when the port is
         * 80 and there is no context path.
         */
        return !UserAgentUtil.isOsxFinder(userAgent) && StringUtils.isEmpty(request.getContextPath());
        /* Since only the OSX Finder cannot work with this proxy, we'll just handle anything that is not Finder.
         * This is required because MS Office sometimes send a Mozilla/XXX user agent header and when that happens and
         * this method returns false, shit happens.
         *
         * I'm open to any good suggestions.
         */
    }

    public void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain
            filterChain) throws IOException, ServletException {
        String originalUri = httpServletRequest.getRequestURI();
        if (StringUtils.startsWith(originalUri, mountPointPrefix)) {
            final String targetPath = new StringBuilder()
                    .append(getPrefix())
                    .append(originalUri.substring(mountPointPrefix.length()))
                    .toString();
            logger.debug(String.format("Forwarding webdav request to WebDavServlet on path :%s", targetPath));
            filterConfig.getServletContext().getRequestDispatcher(targetPath).forward(httpServletRequest,
                    httpServletResponse);
            return;
        }

        if (StringUtils.equalsIgnoreCase(httpServletRequest.getMethod(), DavMethods.METHOD_OPTIONS)
                && StringUtils.equals("/", originalUri)) {
            httpServletResponse.addHeader("MS-Author-Via",
                    WebdavConstants.HEADER_DAV); /* Make Windows don't think of us as NTLM aware */
            httpServletResponse.setStatus(HttpServletResponse.SC_OK); /* Just reply if OPTIONS and / */
            return;
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}
