package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Represents the <code>Global</code> directory. Its immediate children
 * will be directories representing global spaces.
 *
 * @author weiching.cher
 */
public class GlobalSpacesResourceImpl extends AbstractCollectionResource {
    public static final String DISPLAY_NAME = "Global";

    private final SpaceManager spaceManager;

    public GlobalSpacesResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SpaceManager spaceManager) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.spaceManager = spaceManager;
    }

    protected long getCreationtTime() {
        return 0;
    }

    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @Override
    public boolean isValidated() {
        // Permissions for individual spaces are already respected in the SpaceQuery we construct in the
        // getMemberResources method below.
        return true;
    }

    public Collection<DavResource> getMemberResources() {
        try {
            List<DavResource> members = new ArrayList<DavResource>();
            DavResourceFactory davResourceFactory = getFactory();
            DavResourceLocator locator = getLocator();
            @SuppressWarnings("unchecked")
            List<Space> globalSpaces = spaceManager.getAllSpaces(
                    SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).withSpaceType(SpaceType.GLOBAL).build());

            for (Space space : globalSpaces) {
                StringBuilder resourcePathBuffer = new StringBuilder();
                resourcePathBuffer.append("/" + DISPLAY_NAME + "/").append(space.getKey());

                members.add(
                        davResourceFactory.createResource(
                                locator.getFactory().createResourceLocator(
                                        locator.getPrefix(), locator.getWorkspacePath(), resourcePathBuffer.toString(), false
                                ),
                                getSession()
                        )
                );
            }

            return members;
        } catch (final DavException de) {
            throw new RuntimeException(de);
        }
    }
}
