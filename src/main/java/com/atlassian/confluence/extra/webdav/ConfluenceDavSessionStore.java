package com.atlassian.confluence.extra.webdav;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * This interface does the methods for {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSessionProviderImpl}
 * to call to persist tracked user WebDAV sessiosn.
 */
@ParametersAreNonnullByDefault
public interface ConfluenceDavSessionStore {
    /**
     * Store a {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSession} that belongs to the
     * specified user.
     *
     * @param davSession The {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSession} to store.
     * @param userName   The name of the user the session belongs to.
     */
    void mapSession(ConfluenceDavSession davSession, String userName);

    /**
     * Gets a stored {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSession} belonging to a user.
     *
     * @param userName The session owner name
     * @return The stored session or <code>null</code> if there is no session stored for the specified user name.
     */
    @Nullable
    ConfluenceDavSession getSession(String userName);

    /**
     * Invalidates expired sessions from the store.
     */
    void invalidateExpiredSessions();

    /**
     * Executes a task on every currently active sessions held in the store.
     *
     * @param confluenceDavSessionTask The task to be executed.
     *                                 It's {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSessionTask#execute(ConfluenceDavSession)}
     *                                 method will be called for each active session.
     */
    void executeTaskOnSessions(ConfluenceDavSessionTask confluenceDavSessionTask);
}
