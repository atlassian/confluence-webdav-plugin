package com.atlassian.confluence.extra.webdav;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * A cluster-safe implementation of {@link ConfluenceDavSessionStore} based on atlassian-cache.
 *
 * @deprecated since 5.0.0 - Use {@link BandanaConfluenceDavSessionStore}
 */
@Deprecated
@ParametersAreNonnullByDefault
public class DefaultConfluenceDavSessionStore implements ConfluenceDavSessionStore {
    private static final Logger logger = LoggerFactory.getLogger(DefaultConfluenceDavSessionStore.class);

    private static final String CACHE_KEY = "com.atlassian.confluence.extra.webdav.sessions";

    private final CacheManager cacheManager;

    private final long sessionTimeoutInMillis;

    private static final long DEFAULT_SESSION_TIMEOUT = 1800000;

    /**
     * Instatiates a {@link com.atlassian.confluence.extra.webdav.DefaultConfluenceDavSessionStore}
     * with a specific session timeout value.
     *
     * @param cacheManager           The {@link com.atlassian.cache.CacheManager} that wil be used to store the instances of
     *                               {@link ConfluenceDavSessionStore} managed by  this class.
     * @param sessionTimeoutInMillis The amount time that must a session must remain inactive before it is elligble for invalidation.
     */
    public DefaultConfluenceDavSessionStore(@ComponentImport CacheManager cacheManager, long sessionTimeoutInMillis) {
        this.cacheManager = cacheManager;
        this.sessionTimeoutInMillis = sessionTimeoutInMillis;
    }

    public DefaultConfluenceDavSessionStore(CacheManager cacheManager) {
        this(cacheManager, DEFAULT_SESSION_TIMEOUT);
    }

    private Cache<String, ConfluenceDavSession> getSessionCache() {
        return cacheManager.getCache(CACHE_KEY);
    }

    private void cacheSession(String userName, ConfluenceDavSession confluenceDavSession) {
        Cache<String, ConfluenceDavSession> sessionCache = getSessionCache();
        sessionCache.put(userName, confluenceDavSession);
    }

    private ConfluenceDavSession getCachedSession(String userName) {
        return getCachedSession(getSessionCache(), userName);
    }

    private ConfluenceDavSession getCachedSession(Cache<String, ConfluenceDavSession> sessionCache, String userName) {
        try {
            return sessionCache.get(userName);
        } catch (ClassCastException cce) {
            // If this happens, either someone stored something that is not a ConfluenceDavSession into the cache with the same key
            // or the plugin was upgraded and the ConfluenceDavSession loaded by the previous class loader can't be cast to
            // a ConfluenceDavSession
            logger.warn("Unable to cast the cached session of user " + userName + " to a ConfluenceDavSession. It will be purged from the cache.", cce);
            sessionCache.remove(userName);
        }

        return null;
    }


    public void mapSession(ConfluenceDavSession davSession, String userName) {
        cacheSession(userName, davSession);
    }

    public ConfluenceDavSession getSession(String userName) {
        return getCachedSession(userName);
    }


    private boolean isSessionExpired(ConfluenceDavSession confluenceDavSession) {
        return System.currentTimeMillis() - confluenceDavSession.getLastActivityTimestamp() > sessionTimeoutInMillis
                && !confluenceDavSession.isCurrentlyBeingUsed();
    }

    public void invalidateExpiredSessions() {
        Cache<String, ConfluenceDavSession> sessionCache = getSessionCache();
        Collection<String> uniqueSessionKeys = sessionCache.getKeys();
        int sessionSize = uniqueSessionKeys.size();

        logger.debug("Number of sessions before invalidation: " + sessionSize);

        if (sessionSize > 100)
            logger.warn("There are " + sessionSize +
                    " active WebDAV sessions just before invalidation. Just thought of telling you that because there seems to be an unusual number of users using the WebDAV plugin.");

        for (String sessionKey : uniqueSessionKeys) {
            ConfluenceDavSession confluenceDavSession = getCachedSession(sessionCache, sessionKey);

            if (null != confluenceDavSession && isSessionExpired(confluenceDavSession))
                sessionCache.remove(sessionKey);
        }

        logger.debug("Number of sessions after invalidation: " + sessionCache.getKeys().size());
    }

    public void executeTaskOnSessions(ConfluenceDavSessionTask confluenceDavSessionTask) {
        Cache<String, ConfluenceDavSession> sessionCache = getSessionCache();
        Collection<String> uniqueSessionKeys = sessionCache.getKeys();

        for (String sessionKey : uniqueSessionKeys) {
            ConfluenceDavSession confluenceDavSession = sessionCache.get(sessionKey);

            confluenceDavSessionTask.execute(confluenceDavSession);
            /* Update the cache with possibly modified session */
            if (confluenceDavSession != null) {
                mapSession(confluenceDavSession, confluenceDavSession.getUserName());
            }
        }
    }
}
