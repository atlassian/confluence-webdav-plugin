package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents a non-visible resource, the dashboard. The dashboard resource is the
 * parent resource for the <code>Global</code> and <code>Personal</code> directories.
 *
 * @author weiching.cher
 */
public class DashboardResourceImpl extends AbstractCollectionResource {
    private final String workspaceName;

    public DashboardResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            String workspaceName) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.workspaceName = workspaceName;
    }

    protected long getCreationtTime() {
        return 0;
    }

    public String getDisplayName() {
        return workspaceName;
    }

    protected Collection<DavResource> getMemberResources() {
        try {
            Collection<DavResource> memberResources = new ArrayList<DavResource>();
            DavResourceFactory davResourceFactory = getFactory();
            DavResourceLocator locator = getLocator();

            memberResources.add(
                    davResourceFactory.createResource(
                            locator.getFactory().createResourceLocator(locator.getPrefix(), locator.getWorkspacePath(), "/" + GlobalSpacesResourceImpl.DISPLAY_NAME, false),
                            getSession()
                    )
            );
            memberResources.add(
                    davResourceFactory.createResource(
                            locator.getFactory().createResourceLocator(locator.getPrefix(), locator.getWorkspacePath(), "/" + PersonalSpacesResourceImpl.DISPLAY_NAME, false),
                            getSession()
                    )
            );

            return memberResources;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
