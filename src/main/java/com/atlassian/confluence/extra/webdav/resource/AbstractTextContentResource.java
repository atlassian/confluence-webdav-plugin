package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


public abstract class AbstractTextContentResource extends AbstractContentResource {
    public static final String CONTENT_TYPE = "text/plain";

    private final SettingsManager settingsManager;

    public AbstractTextContentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
        this.settingsManager = settingsManager;
    }

    protected SettingsManager getSettingsManager() {
        return settingsManager;
    }

    protected abstract byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException;

    protected String getContentTypeBase() {
        return CONTENT_TYPE;
    }

    protected String getContentType() {
        return getContentTypeBase();
    }

    protected InputStream getContent() {
        try {
            return new ByteArrayInputStream(
                    getTextContentAsBytes(
                            settingsManager.getGlobalSettings().getDefaultEncoding()));
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(uee);
        }
    }

    protected long getContentLength() {
        try {
            return getTextContentAsBytes(
                    settingsManager.getGlobalSettings().getDefaultEncoding()).length;
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(uee);
        }
    }
}
