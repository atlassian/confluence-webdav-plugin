package com.atlassian.confluence.extra.webdav;

import org.apache.jackrabbit.webdav.AbstractLocatorFactory;
import org.apache.jackrabbit.webdav.simple.LocatorFactoryImplEx;

/**
 * This class provides a way to undo the affects of changes done in {@link LocatorFactoryImplEx} as part of JCR-1679.
 * The changes can be found here: https://svn.apache.org/viewvc?view=revision&amp;revision=677895
 */
public class ConfluenceLocatorFactory extends LocatorFactoryImplEx {

    public ConfluenceLocatorFactory(String pathPrefix) {
        super(pathPrefix);
    }

    /**
     *
     * @see AbstractLocatorFactory#getResourcePath(String, String)
     */
    @Override
    protected String getResourcePath(String repositoryPath, String wspPath) {
        if (repositoryPath == null) {
            throw new IllegalArgumentException("Cannot build resource path from 'null' repository path");
        }
        return (startsWithWorkspace(repositoryPath, wspPath)) ? repositoryPath : wspPath + repositoryPath;
    }

    private boolean startsWithWorkspace(String repositoryPath, String wspPath) {
        if (wspPath == null) {
            return true;
        } else {
            return repositoryPath.startsWith(wspPath + "/");
        }
    }
}
