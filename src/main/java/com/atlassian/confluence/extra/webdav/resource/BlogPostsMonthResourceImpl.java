package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents the blog posts month directory. For instance:
 * <code>/Global/ds/&amp;#064;news/2008/01</code>
 *
 * @author weiching.cher
 */
public class BlogPostsMonthResourceImpl extends BlogPostsYearResourceImpl {
    private final UserAccessor userAccessor;

    protected final int monthPublished;

    public BlogPostsMonthResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport UserAccessor userAccessor,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            int yearPublished,
            int monthPublished) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, permissionManager, spaceManager, pageManager, spaceKey, yearPublished);
        this.userAccessor = userAccessor;
        this.monthPublished = monthPublished;
    }

    public String getDisplayName() {
        return new DecimalFormat("00").format(monthPublished);
    }

    protected Calendar getUserDateWithTimeZone() {
        return Calendar.getInstance(
                userAccessor.getConfluenceUserPreferences(
                        AuthenticatedUserThreadLocal.getUser()
                ).getTimeZone().getWrappedTimeZone()
        );
    }

    protected Calendar getBlogPostPeriod() {
        Calendar dateWithUserTimeZone = getUserDateWithTimeZone();

        dateWithUserTimeZone.set(yearPublished, monthPublished - 1, 1);
        return dateWithUserTimeZone;
    }

    public Collection<DavResource> getMemberResources() {
        try {
            DavResourceLocator locator = getLocator();
            String parentPath = getParentResourcePath();
            StringBuffer childResourcePathBuffer = new StringBuffer();
            Set<String> uniqueChildPaths = new HashSet<String>();
            List<DavResource> members = new ArrayList<DavResource>();
            Calendar blogPostsPublishedDate;
            List<BlogPost> blogPosts;

            blogPostsPublishedDate = getBlogPostPeriod();

            blogPosts = getPermissionManager().getPermittedEntities(
                    AuthenticatedUserThreadLocal.getUser(),
                    Permission.VIEW,
                    getPageManager().getBlogPosts(spaceKey, blogPostsPublishedDate, Calendar.MONTH)
            );

            for (BlogPost blogPost : blogPosts) {
                childResourcePathBuffer.setLength(0);
                childResourcePathBuffer.append(parentPath)
                        .append('/').append(blogPost.getPostingMonthNumeric())
                        .append('/').append(blogPost.getPostingDayOfMonth());

                uniqueChildPaths.add(childResourcePathBuffer.toString());
            }

            for (String uniqueChildPath : uniqueChildPaths) {
                DavResourceLocator blogPostDayResourceLocator =
                        locator.getFactory().createResourceLocator(
                                locator.getPrefix(),
                                locator.getWorkspacePath(),
                                uniqueChildPath,
                                false);

                members.add(getFactory().createResource(blogPostDayResourceLocator, getSession()));
            }

            return members;
        } catch (DavException de) {
            throw new RuntimeException(de);
        }
    }
}
