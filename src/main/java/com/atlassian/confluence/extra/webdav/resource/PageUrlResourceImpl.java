package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;

/**
 * Represents the .url file of a page. For instance:
 * <code>/Global/ds/myPage/myPage.url</code>.
 *
 * @author weiching.cher
 */
public class PageUrlResourceImpl extends AbstractTextContentResource {
    public static final String CONTENT_TYPE = "text/url";

    public static final String DISPLAY_NAME_SUFFIX = ".url";

    private final WebdavSettingsManager webdavSettingsManager;

    private final PageManager pageManager;

    private final String spaceKey;

    private final String pageTitle;

    public PageUrlResourceImpl(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager,
            WebdavSettingsManager webdavSettingsManager,
            @ComponentImport PageManager pageManager,
            String spaceKey,
            String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
        this.webdavSettingsManager = webdavSettingsManager;
        this.pageManager = pageManager;
        this.spaceKey = spaceKey;
        this.pageTitle = pageTitle;
    }

    public Page getPage() {
        return pageManager.getPage(spaceKey, pageTitle);
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentUrlHidden(getPage())
                && webdavSettingsManager.isContentUrlResourceEnabled();
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return new StringBuffer("[InternetShortcut]\r\n")
                .append("URL=")
                .append(getSettingsManager().getGlobalSettings().getBaseUrl()).append(getPage().getUrlPath()).append("\r\n")
                .toString().getBytes(encoding);
    }

    protected String getContentTypeBase() {
        return CONTENT_TYPE;
    }

    public long getModificationTime() {
        return getPage().getLastModificationDate().getTime();
    }

    protected long getCreationtTime() {
        return getPage().getCreationDate().getTime();
    }

    public String getDisplayName() {
        return getPage().getTitle() + DISPLAY_NAME_SUFFIX;
    }
}
