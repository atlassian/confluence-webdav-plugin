package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.apache.struts2.ServletActionContext;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceIteratorImpl;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.Scope;
import org.apache.jackrabbit.webdav.lock.SupportedLock;
import org.apache.jackrabbit.webdav.lock.Type;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.DefaultDavProperty;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;

/**
 * Abstract representation of all file resources (e.g. page content, attachment, version text files, content exports).
 */
public abstract class AbstractContentResource extends AbstractConfluenceResource {
    private static final Predicate<Map.Entry<String, String>> NOT_CONTENT_TYPE_HEADERS = new Predicate<Map.Entry<String, String>>() {
        @Override
        public boolean apply(@Nonnull Map.Entry<String, String> input) {
            return !"Content-Type".equals(input.getKey());
        }
    };

    public AbstractContentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession);
    }

    protected abstract InputStream getContent();

    protected abstract String getContentType();

    protected abstract long getContentLength();

    /**
     * @return a map of http headers to add to the response.
     */
    protected Map<String, String> getHeaders() {
        return Collections.emptyMap();
    }

    protected SupportedLock getSupportedLock() {
        SupportedLock supportedLock = new SupportedLock();

        supportedLock.addEntry(Type.WRITE, Scope.EXCLUSIVE);

        return supportedLock;
    }

    /**
     * {@inheritDoc}
     *
     * @return Always returns <code>false</code>, since this resource represents a file.
     */
    public boolean isCollection() {
        return false;
    }

    protected void initProperties(DavPropertySet propertySet) {
        super.initProperties(propertySet);

        propertySet.add(new DefaultDavProperty(DavPropertyName.GETCONTENTLENGTH, getContentLength()));
        propertySet.add(new DefaultDavProperty(DavPropertyName.GETCONTENTTYPE, getContentType()));
    }

    public void spool(OutputContext outputContext) throws IOException {
        if (outputContext.hasStream()) {
            try (OutputStream outputStream = outputContext.getOutputStream();
                 InputStream inputStream = getContent()) {

                outputContext.setContentLength(getContentLength());
                outputContext.setContentType(getContentType());
                outputContext.setModificationTime(getModificationTime());

                //we set all the headers returned by getHeaders(), except content-type as it is already set above. This
                //is normally going to be an empty set, except for attachment resources, which would contain the same
                //set of headers as the attachment download from a normal confluence page. This prevents XSS using webdav
                for (Map.Entry<String, String> header : Maps.filterEntries(getHeaders(), NOT_CONTENT_TYPE_HEADERS).entrySet()) {
                    ServletActionContext.getResponse().setHeader(header.getKey(), header.getValue());
                }

                if (inputStream != null && outputStream != null)
                    IOUtils.copy(inputStream, outputStream);
            }
        } else {
            outputContext.setContentLength(0);
            outputContext.setModificationTime(getModificationTime());
        }
    }

    /**
     * Returns the child resources of this resource.
     *
     * @return An empty {@link org.apache.jackrabbit.webdav.DavResourceIterator}. Since this resource represents a file
     * in WebDAV.
     */
    public final DavResourceIterator getMembers() {
        return new DavResourceIteratorImpl(Collections.EMPTY_LIST);
    }
}
