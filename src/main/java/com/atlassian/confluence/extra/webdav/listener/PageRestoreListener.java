package com.atlassian.confluence.extra.webdav.listener;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSessionTask;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Listens for page restore events coming from Confluence to reset appropriate flags
 * in {@link com.atlassian.confluence.extra.webdav.ResourceStates}. This is useful when WebDAV resources
 * removed by clients are restored via Confluence. When content is restored, we want all of its states reset.
 * <p>
 * This class does that.
 *
 * @see com.atlassian.confluence.extra.webdav.ResourceStates
 */
public final class PageRestoreListener {

    private final EventPublisher eventPublisher;
    private final ConfluenceDavSessionStore confluenceDavSessionStore;

    @Autowired
    public PageRestoreListener(
            final EventPublisher eventPublisher,
            final ConfluenceDavSessionStore confluenceDavSessionStore
    ) {
        this.eventPublisher = eventPublisher;
        this.confluenceDavSessionStore = confluenceDavSessionStore;
    }

    @PostConstruct
    public void setup() {
        this.eventPublisher.register(this);
    }

    @PreDestroy
    public void teardown() {
        this.eventPublisher.unregister(this);
    }

    @EventListener
    public void handleEvent(final PageRestoreEvent event) {
        confluenceDavSessionStore.executeTaskOnSessions(
                new ContentEntityAttributesResetDavSessionTask(event.getContent())
        );
    }

    private static class ContentEntityAttributesResetDavSessionTask implements ConfluenceDavSessionTask {
        private final ContentEntityObject ceo;

        private ContentEntityAttributesResetDavSessionTask(ContentEntityObject ceo) {
            this.ceo = ceo;
        }

        public void execute(final ConfluenceDavSession confluenceDavSession) {
            confluenceDavSession.getResourceStates().resetContentAttributes(ceo);
        }
    }

}
