package it.com.atlassian.confluence.extra.webdav;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.HttpDelete;
import org.apache.jackrabbit.webdav.client.methods.HttpLock;
import org.apache.jackrabbit.webdav.client.methods.HttpPropfind;
import org.apache.jackrabbit.webdav.client.methods.HttpUnlock;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * Automated Functional Test
 * <p>
 * This class will be testing operations based on MacOS Finder behaviors.
 * Test includes update page content, delete page, update space description and upload attachment
 *
 * @author weiching.cher
 */
public class MacWebDavTestCase extends AbstractWebDavTestCase {
    /**
     * Test update space description with MacOS Finder
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMacGlobalUpdateSpaceDescriptionAsFolders() throws IOException, DavException {
        // Propfind demonstration space.txt
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        assertPropFindHasResults(targetUrl, singletonList(
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));

        // Delete demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);
        deleteMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        // Propfind Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpPropfind httpPropfind = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);
        httpPropfind.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(httpPropfind, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NOT_FOUND, statusCode);

        // Put Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        putMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        StringEntity requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpStatus.SC_CREATED, statusCode);

        // Propfind ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        httpPropfind = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        httpPropfind.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        try (CloseableHttpResponse response = httpClient.execute(httpPropfind, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpStatus.SC_NOT_FOUND, statusCode);

        // Put ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        putMethod = new HttpPut(targetUrl);
        putMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_CREATED, statusCode);

        // Lock ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        String[] lockTokens = {};
        HttpLock lockMethod = new HttpLock(targetUrl, 0, lockTokens);
        lockMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(lockMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);

        // Put ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        putMethod = new HttpPut(targetUrl);
        putMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        // Unlock ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        HttpUnlock unlockMethod = new HttpUnlock(targetUrl, targetUrl);
        unlockMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(unlockMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);

        // Lock ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        lockMethod = new HttpLock(targetUrl, 0, lockTokens);
        lockMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(lockMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);

        // Unlock ._Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        unlockMethod = new HttpUnlock(targetUrl, targetUrl);
        unlockMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(unlockMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);

        // Delete ._Demonstration Space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";
        deleteMethod = new HttpDelete(targetUrl);
        deleteMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        // Get Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpGet getMethod = new HttpGet(targetUrl);
        getMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_OK, statusCode);

        // Put Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        putMethod = new HttpPut(targetUrl);
        putMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        // Propfind Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        httpPropfind = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        httpPropfind.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        assertPropFindHasResults(targetUrl, singletonList(
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));
        
        // Unlock Demonstration space.txt
        targetUrl = getWebdavServletUrl() + "/Global/ds/._Demonstration%20Space.txt";

        unlockMethod = new HttpUnlock(targetUrl, targetUrl);
        unlockMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        try (CloseableHttpResponse response = httpClient.execute(unlockMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);

        // Propfind ds x 2
        targetUrl = getWebdavServletUrl() + "/Global/ds";
        httpPropfind = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        httpPropfind.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));
    }

    private void assertPropFindHasResults(String targetUrl, List<String> expectedMultiStatusResponseHrefs) throws IOException, DavException {
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);
        propFindMethod.setHeader("User-Agent", "WebDAVFS/1.4.3 (01438000) Darwin/8.11.1 (i386)");

        MultiStatus multiStatus;
        MultiStatusResponse[] multiStatusResponses;
        List actualMultiStatusResponseHrefs;
        try (CloseableHttpResponse response = httpClient.execute(propFindMethod, httpClientContext)) {
            multiStatus = propFindMethod.getResponseBodyAsMultiStatus(response);
        }
        multiStatusResponses = multiStatus.getResponses();
        assertNotNull(multiStatusResponses);
        assertEquals(expectedMultiStatusResponseHrefs.size(), multiStatusResponses.length);
        actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

        assertThat(expectedMultiStatusResponseHrefs, hasSize(actualMultiStatusResponseHrefs.size()));
        assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
    }
}
