package it.com.atlassian.confluence.extra.webdav;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.jackrabbit.webdav.client.methods.HttpOptions;

import java.io.IOException;
import java.util.Set;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;

public class WindowsWebDavTestCase extends AbstractWebDavTestCase {
    /**
     * Tests to make sure that the confluence webdav servlet will be called if the
     * request is for the webdav root and there is no ending '/'. This is how XP web
     * folders does discovery.
     */
    public void testNoEndingPathSeparatorWorks() throws IOException {
        String webDavRootPath = "/plugins/servlet/confluence";
        String targetUrl = new StringBuffer("http://")
                .append(getWebdavServerHostName())
                .append(':').append(getWebdavServerPort())
                .append('/').append(getWebdavServerContextPath())
                .append(webDavRootPath).toString();

        HttpOptions optionsMethod = new HttpOptions(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(optionsMethod, httpClientContext)) {
            Set<String> allowedMethods = optionsMethod.getAllowedMethods(response);
            assertThat(allowedMethods, hasItems("LOCK", "UNLOCK", "PROPFIND", "PUT", "GET"));
        }
    }

    /**
     * Tests to make sure that the Confluence index will be updated after
     * making a page content update from WebDAV
     */
    public void testConfluenceIndexAfterUpdatePageContent() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.txt";
        HttpPut putMethod = new HttpPut(targetUrl);

        String pageContent = "There is something now";

        StringEntity stringEntity = new StringEntity(pageContent);
        putMethod.setEntity(stringEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);

        pause((1000 * 60) + (1000 * 30));

        gotoPage("/display/ds/Index");
        assertLinkPresentWithText("Index");
        assertTextPresent(pageContent);
    }
}
