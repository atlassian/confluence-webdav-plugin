package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.HttpCopy;
import org.apache.jackrabbit.webdav.client.methods.HttpMkcol;
import org.apache.jackrabbit.webdav.client.methods.HttpMove;
import org.apache.jackrabbit.webdav.client.methods.HttpPropfind;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static java.util.Calendar.MAY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Automated Functional Test
 *
 * This class will be testing operations based on folder manipulation. Such as fetch/create/move/delete/copy page.
 *
 * @author weiching.cher
 *
 */
public class FolderListWebDavTestCase extends AbstractWebDavTestCase {
    // ------ <  Global  > ----------------------------------------------------------------
    // ###### < READING  > ################################################################

    /**
     * Test getting the top level folders, &quot;Global&quot; and &quot;Personal&quot;.
     */
    public void testGetTopLevelFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl();
        HttpPropfind propFindMethod = new HttpPropfind(targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/",
                getWebdavServletUrl() + "/Personal/",
                getWebdavServletUrl() + "/"));

    }

    /**
     * Test getting the global space keys as folders under &quot;/Global&quot;.
     */
    public void testGetGlobalSpacesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/",
                getWebdavServletUrl() + "/Global/ds/"));
    }

    /**
     * Test PROPFIND command by browsing to news (blog) directory.
     */
    public void testGetGlobalBlogAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/@news/2004/11/21";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/",
                getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/Octagon%20blog%20post.txt"));
    }

    /**
     * Test PROPFIND command by browsing Global Space directory
     */
    public void testGetGlobalPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * Test PROPFIND command by browsing child page.
     */
    public void testGetGlobalChildPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc"));
    }

    /**
     * Test PROPFIND command by browsing Global Space to check for newly added space.
     */
    public void testGetGlobalNewSpaceAsFolders() throws IOException, DavException {
        SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey("spaceofspace");
        spaceHelper.setName("Space of space");

        assertTrue(spaceHelper.create());

        //gotoPage("/spaces/browsespace.action?key=" + spaceHelper.getKey());

        String targetUrl = getWebdavServletUrl() + "/Global";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            assertPropFindHasResults(propFindMethod, Arrays.asList(
                    getWebdavServletUrl() + "/Global/",
                    getWebdavServletUrl() + "/Global/ds/",
                    getWebdavServletUrl() + "/Global/spaceofspace/"));
        } finally {
            spaceHelper.delete();
        }
    }

    /**
     * Test PROPFIND command by browsing newly created page.
     */
    public void testGetGlobalNewPageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Page of page");
        pageHelper.setContent("page content");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            assertPropFindHasResults(propFindMethod, Arrays.asList(
                    getWebdavServletUrl() + "/Global/ds/",
                    getWebdavServletUrl() + "/Global/ds/%40news/",
                    getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                    getWebdavServletUrl() + "/Global/ds/Index/",
                    getWebdavServletUrl() + "/Global/ds/Page%20of%20page/",
                    getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
        } finally {
            pageHelper.delete();
        }
    }

    /**
     * Test PROPFIND command by browsing newly created news (blog).
     */
    public void testGetGlobalNewBlogAsFolders() throws IOException, DavException {
        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2008, MAY, 15);

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("Blog of blog");
        blogPostHelper.setContent("blog content");

        assertTrue(blogPostHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/@news/2008/05/15";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            assertPropFindHasResults(propFindMethod, Arrays.asList(
                    getWebdavServletUrl() + "/Global/ds/%40news/2008/05/15/",
                    getWebdavServletUrl() + "/Global/ds/%40news/2008/05/15/Blog%20of%20blog.txt"));
        } finally {
            blogPostHelper.delete();
        }
    }

    /**
     * WBDV-87
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-87
     */
    public void testGetGlobalExportAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.pdf",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.doc",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/README.txt"));
    }

    /**
     * Test MKCOL command by creating Scandinavian Characters as page title
     * Test PROPFIND command by browsing page with Scandinavian Characters
     */
    public void testGetGlobalScandinavianCharactersAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t/"));
    }

    // ------ < Personal > ----------------------------------------------------------------
    /**
     * Test getting the personal space keys as folders under &quot;/Personal&quot;.
     */
    public void testGetPersonalSpacesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/",
                getWebdavServletUrl() + "/Personal/~admin/"));
    }

    /**
     * Test PROPFIND command by browsing page in Personal Space.
     */
    public void testGetPersonalPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));
    }

    /**
     * Test PROPFIND command by browsing page versions folder in Personal Space.
     */
    public void testGetPersonalVersionsAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40versions";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%201.txt",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%202.txt",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%203.txt",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/README.txt"));
    }

    /**
     * Test PROPFIND command by browsing Personal Space's news (blog).
     */
    public void testGetPersonalBlogAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/%40news";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/2008/"));
    }

    /**
     * Test PROFIND command by browsing page exports folder in Personal Space.
     */
    public void testGetPersonalExportAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40exports";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/Home.pdf",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/Home.doc",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/README.txt"));
    }

    // ###### < WRITING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test MKCOL command by creating new page in Global Space.
     */
    public void testMkColGlobalCreateNewPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * Test DELETE command by removing page in Global Space.
     */
    public void testDelGlobalRemovePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * WBDV-41
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-41
     */
    public void testMoveGlobalRenamePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/pagenew/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * Test MOVE command by renaming page to another page that has the same name in Global Space.
     */
    public void testMoveGlobalRenamePageToExistingPageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Index2");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Index2/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/Index2";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Index";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_PRECONDITION_FAILED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/Index2/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * Test COPY command by copying page to same location.
     */
    public void testCopyGlobalPageAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        // TODO verify copy page hierarchy in WebDAV
        HttpCopy copyMethod = new HttpCopy(targetUrl, destinationUrl, false, true);

        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        HttpPropfind propFindMethod;
        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Creating%20pages%20and%20linking%20(2).txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Creating%20pages%20and%20linking%20(2).url"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                "/Copy%20of%20Breadcrumb%20demonstration";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/Copy%20of%20Breadcrumb%20demonstration.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/Copy%20of%20Breadcrumb%20demonstration.url"));
    }


    /**
     * Test COPY command by copying page to same location multiple times.
     */
    public void testCopyGlobalPageMultipleTimesAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and" +
                "%20linking%20(2)";
        HttpCopy copyMethod = new HttpCopy(targetUrl, destinationUrl, false, false);

        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Copy%20of%20Breadcrumb%20demonstration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Creating%20pages%20and%20linking%20(2).txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)" +
                        "/Creating%20pages%20and%20linking%20(2).url"));


        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)";
        copyMethod = new HttpCopy(targetUrl, destinationUrl, false, false);

        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc"));

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)" +
                        "/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)" +
                        "/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)" +
                        "/Copy%20of%20Copy%20of%20Breadcrumb%20demonstration/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)" +
                        "/Creating%20pages%20and%20linking%20(3).txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)" +
                        "/Creating%20pages%20and%20linking%20(3).url"));
    }

    /**
     * WBDV-113
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-113
     */
    public void testMkColGlobalCreateNewUmlautsCharacterPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%C3%84%C3%BC%C3%9F%C3%B6%C3%A4%C3%96%C3%9C";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/%c3%84%c3%bc%c3%9f%c3%b6%c3%a4%c3%96%c3%9c/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * WBDV-108
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-108
     */
    public void testMkColGlobalCreateNewScandinavianCharacterPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/sv%C3%ADnakj%C3%B6t";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    /**
     * Test MKCOL command by creating new non ascii page in Global Space.
     */
    public void testMkColGlobalCreateNewNonAsciiPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%E5%A5%BD";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/%e5%a5%bd/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"));
    }

    // ## < TEST PERMISSIONS > ##
    /**
     * Test MKCOL command without create permission.
     */
    public void testMkColGlobalTestCreatePagePermissionAsFolders() throws IOException {


        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }

    private HttpClientContext getNotPermittedContext() {
        SystemDefaultCredentialsProvider credentialsProvider = new SystemDefaultCredentialsProvider();
        HttpHost host = new HttpHost(getWebdavServerHostName(),
                getWebdavServerPort(),
                AuthScope.ANY_REALM);
        credentialsProvider.setCredentials(
                new AuthScope(host),
                new UsernamePasswordCredentials("sakura", "sakura")
        );
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicScheme = new BasicScheme();
        authCache.put(host, basicScheme);

        HttpClientContext notPermittedContext = HttpClientContext.create();
        notPermittedContext.setCredentialsProvider(credentialsProvider);
        notPermittedContext.setAuthCache(authCache);
        return notPermittedContext;
    }

    /**
     * Test DELETE command without delete permission.
     */
    public void testDelGlobalTestRemovePagePermissionAsFolders() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }

    /**
     * Test MOVE command without rename permission.
     */
    public void testMoveGlobalTestRenamePagePermissionAsFolders() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Index2";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        try (CloseableHttpResponse response = httpClient.execute(moveMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }


    // ------ < Personal > ----------------------------------------------------------------
    /**
     * Test MKCOL command by creating new page in Personal Space.
     */
    public void testMkColPersonalCreateNewPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));
    }

    /**
     * Test DELETE command by removing page in Personal Space.
     */
    public void testDelPersonalRemovePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));
    }

    /**
     * Test MOVE command by renaming page in Personal Space.
     */
    public void testMovePersonalRenamePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/pagenew";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, true);

        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/pagenew/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));
    }

    /**
     * Test COPY command by copying page to same location in Personal Space.
     */
    public void testCopyPersonalPageAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/Home%20(2)";
        HttpCopy copyMethod = new HttpCopy(targetUrl, destinationUrl, false, false);

        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        assertPropFindHasResults(propFindMethod, Arrays.asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/Home%20(2)/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"));
    }

    // ## < TEST PERMISSIONS > ##
    /**
     * Test MKCOL command without create permission in Personal Space.
     */
    public void testMkColPersonalTestCreatePagePermissionAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }

    /**
     * Test DELETE command without remove permission.
     */
    public void testDelPersonalTestRemovePagePermissionAsFolders() throws IOException, DavException {

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }

    /**
     * Test MOVE command without rename permission.
     */
    public void testMovePersonalTestRenamePagePermissionAsFolders() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/Home2";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        try (CloseableHttpResponse response = httpClient.execute(moveMethod, getNotPermittedContext())) {
            int statusCode = response.getStatusLine().getStatusCode();
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        }
    }

    private void assertPropFindHasResults(HttpPropfind propFindMethod, List<String> expectedMultiStatusResponseHrefs) throws IOException, DavException {
        MultiStatus multiStatus;
        MultiStatusResponse[] multiStatusResponses;
        List<String> actualMultiStatusResponseHrefs;
        try (CloseableHttpResponse response = httpClient.execute(propFindMethod, httpClientContext)) {

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus(response);
        }
        multiStatusResponses = multiStatus.getResponses();
        assertNotNull(multiStatusResponses);
        assertEquals(expectedMultiStatusResponseHrefs.size(), multiStatusResponses.length);

        actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

        assertThat(expectedMultiStatusResponseHrefs, hasSize(actualMultiStatusResponseHrefs.size()));
        assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
    }
}
