package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.jackrabbit.webdav.MultiStatusResponse;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * This class will define all the necessary settings to be used when this class is extended
 *
 * @author weiching.cher
 */
public abstract class AbstractWebDavTestCase extends AbstractConfluencePluginWebTestCase {
    protected CloseableHttpClient httpClient;
    protected HttpClientContext httpClientContext;
    protected HttpClientConnectionManager httpConnectionManager;
    protected SystemDefaultCredentialsProvider credentialsProvider;

    protected void setUp() throws Exception {
        super.setUp();
        setupHttpClient();
    }

    protected void tearDown() throws Exception {
        try {
            httpClient.close();
        } finally {
            super.tearDown();
        }
    }

    protected void setupHttpClient() {
        httpConnectionManager = new BasicHttpClientConnectionManager();
        HttpClientBuilder builder = HttpClientBuilder.create()
                .setConnectionManager(httpConnectionManager);
        this.httpClient = builder.build();
        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();

        credentialsProvider = new SystemDefaultCredentialsProvider();
        HttpHost host = new HttpHost(getWebdavServerHostName(),
                getWebdavServerPort(),
                AuthScope.ANY_REALM);
        credentialsProvider.setCredentials(
                new AuthScope(host),
                new UsernamePasswordCredentials(confluenceWebTester.getAdminUserName(),
                        confluenceWebTester.getAdminPassword())
        );
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicScheme = new BasicScheme();
        authCache.put(host, basicScheme);

        this.httpClientContext = HttpClientContext.create();
        httpClientContext.setCredentialsProvider(credentialsProvider);
        httpClientContext.setAuthCache(authCache);
    }

    protected String getWebdavServerHostName() {
        return System.getProperty("webdav.host");
    }

    protected int getWebdavServerPort() {
        return Integer.parseInt(System.getProperty("webdav.port"));
    }

    protected String getWebdavServerContextPath() {
        return System.getProperty("webdav.context");
    }

    protected String getWebdavServletPath() {
        return System.getProperty("webdav.resource.path.prefix");
    }

    protected String getWebdavServletUrl() {
        return "http://" + getWebdavServerHostName() + ':' + getWebdavServerPort() + '/' + getWebdavServerContextPath() + getWebdavServletPath();
    }

    protected List<String> getMultiStatusHrefs(MultiStatusResponse[] multiStatusResponses) {
        return Stream.of(multiStatusResponses).map(MultiStatusResponse::getHref).collect(toList());
    }

    protected void pause(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            // noop.
        }

    }

}
