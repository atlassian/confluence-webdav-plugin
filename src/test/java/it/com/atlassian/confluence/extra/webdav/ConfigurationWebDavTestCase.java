package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.IndexHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.client.methods.HttpCopy;
import org.apache.jackrabbit.webdav.client.methods.HttpDelete;
import org.apache.jackrabbit.webdav.client.methods.HttpMkcol;
import org.apache.jackrabbit.webdav.client.methods.HttpMove;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ConfigurationWebDavTestCase extends AbstractWebDavTestCase {
    private void setDenyUserAgentRegex(String regex) {
        gotoPage("admin/plugins/webdav/config.action");
        setWorkingForm("webdavSettings");

        final String tokenAttribute = "atl_token";

        // get the XSRF token from the form and use it in the gotoPage below
        String token = getElementAttributeByXPath("//input[@name='" + tokenAttribute + "']", "value");

        gotoPage("/admin/plugins/webdav/doconfig.action?denyRegexes=" + regex + "&" + tokenAttribute + "=" + token);
    }

    protected void setUp() throws Exception {
        super.setUp();

        try {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            setDenyUserAgentRegex("Microsoft.*");
        } finally {
            dropEscalatedPrivileges();
        }
    }

    public void testPutDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("updated space details");

        putMethod.setEntity(requestEntity);
        putMethod.setHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");
        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    public void testCopyDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and" +
                "%20linking%20(2)";
        HttpCopy copyMethod = new HttpCopy(targetUrl, destinationUrl, false, false);
        copyMethod.setHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");
        int statusCode;

        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    public void testDeleteDeniedIfClientIsBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);
        deleteMethod.setHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");
        int statusCode;

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    public void testMkcolDeniedIfClientIsBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/ImaginaryPage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);
        mkColMethod.setHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);

    }

    public void testMoveDeniedIfClientIsBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);
        moveMethod.setHeader("User-Agent", "Microsoft Data Access Internet Publishing Provider DAV");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    public void testPutNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("updated space details");

        putMethod.setEntity(requestEntity);
        putMethod.setHeader("User-Agent", "ImaginaryClient/1.0");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    public void testCopyNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and" +
                "%20linking%20(2)";
        HttpCopy copyMethod = new HttpCopy(targetUrl, destinationUrl, false, false);
        copyMethod.setHeader("User-Agent", "ImaginaryClient/1.0");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(copyMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_CREATED, statusCode);
    }

    public void testDeleteNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);
        deleteMethod.setHeader("User-Agent", "ImaginaryClient/1.0");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }

        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    public void testMkcolNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/ImaginaryPage";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);
        mkColMethod.setHeader("User-Agent", "ImaginaryClient/1.0");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_CREATED, statusCode);
    }

    public void testMoveNotDeniedIfClientIsNotBlacklisted() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);
        moveMethod.setHeader("User-Agent", "ImaginaryClient/1.0");

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_CREATED, statusCode);
    }

    public void testCustomWebdavSettingsLoadedFromPluginClassLoaderByXstream() {
        try {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            setDenyUserAgentRegex("Ack");

            IndexHelper indexHelper = getIndexHelper();
            indexHelper.flush();

            gotoPage("/admin/console.action");
            clickLinkWithText("WebDAV Configuration");
        } finally {
            dropEscalatedPrivileges();
        }
    }
}
