package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.HttpDelete;
import org.apache.jackrabbit.webdav.client.methods.HttpMkcol;
import org.apache.jackrabbit.webdav.client.methods.HttpMove;
import org.apache.jackrabbit.webdav.client.methods.HttpPropfind;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Calendar.MAY;
import static java.util.Calendar.NOVEMBER;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * Automated Functional Test
 * <p>
 * This class will be testing operations based on file manipulation.
 * Such as fetch/update/delete page content.
 *
 * @author weiching.cher
 */
public class FileListWebDavTestCase extends AbstractWebDavTestCase {
    private static Logger log = LoggerFactory.getLogger(FileListWebDavTestCase.class);

    // ###### < READING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test GET command to fetch space description content and check the content
     *
     * @throws IOException
     */
    public void testGetGlobalSpaceDetailsAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "A space which demonstrates Confluence functionality.";
        Assert.assertEquals(expectedBody, actualBody);
    }

    /**
     * Test GET command to fetch page content and check the content
     *
     * @throws IOException
     */
    public void testGetGlobalPageAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");
        pageHelper.setVersion(61);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals(content, actualBody);
    }

    /**
     * Test GET command to fetch space news (blog) and check the content
     *
     * @throws IOException
     */
    public void testGetGlobalBlogAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/Octagon%20blog%20post.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2004, NOVEMBER, 21);

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("Octagon blog post");
        blogPostHelper.setId(blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle());

        assertTrue(blogPostHelper.read());

        String content = blogPostHelper.getContent();

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals(content, actualBody);
    }

    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test GET command to fetch personal space page content and check the content
     *
     * @throws IOException
     */
    public void testGetPersonalPageAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("Home");
        pageHelper.setVersion(2);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();
        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals(content, actualBody);
    }

    /**
     * Test GET command to fetch personal space page version content and check the content
     *
     * @throws IOException
     */
    public void testGetPersonalVersionAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%203.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("Home");
        pageHelper.setVersion(3);
        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());

        assertTrue(pageHelper.read());

        String content = pageHelper.getContent();

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals(content, actualBody);
    }

    /**
     * Test GET command to fetch personal space news (blog) and check the content
     *
     * @throws IOException
     */
    public void testGetPersonalBlogAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/%40news/2008/05/15/admin%20blog%20post.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2008, MAY, 15);

        blogPostHelper.setSpaceKey("~admin");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("admin blog post");
        blogPostHelper.setId(blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle());

        assertTrue(blogPostHelper.read());

        String content = blogPostHelper.getContent();

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals(content, actualBody);
    }

    // ###### < WRITING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test PUT command to update space description and check the content
     *
     * @throws IOException
     */
    public void testPutGlobalUpdateSpaceDescriptionAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("updated space details");
        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);

        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream inputStream = response.getEntity().getContent();
            actualBody = IOUtils.toString(inputStream, UTF_8);
        }
        Assert.assertEquals("updated space details", actualBody);
    }

    /**
     * Test REMOVE command to delete attachment from a page
     *
     * @throws IOException
     */
    public void testDelGlobalRemoveAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/spacesummary.gif";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }

        assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/%40exports/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/%40versions/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/Using%20Spaces.txt",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/Using%20Spaces.url"
        ));
    }

    /**
     * Test PUT command to upload an existing attachment to a page
     *
     * @throws IOException
     */
    public void testPutGlobalUploadSameNameAttachmentAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);
        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test PUT command to update page content to a page
     *
     * @throws IOException
     */
    public void testPutGlobalUpdatePageContentAsFile() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");
        pageHelper.setContent("Nothing now");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("There is something now");
        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);

        HttpGet getMethod = new HttpGet(targetUrl);
        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "There is something now";
        Assert.assertEquals(expectedBody, actualBody);
    }

    /**
     * Test PUT command to update page content to a page and check for page version
     *
     * @throws IOException
     */
    public void testPutGlobalUpdatePageContentAndCheckPageVersionAsFile() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newnewpage");
        pageHelper.setContent("Nothing now");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/newnewpage/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40exports/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.txt",
                getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.url"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%201.txt",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/README.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/newnewpage.txt";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("There is something now");

        putMethod.setEntity(requestEntity);
        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        targetUrl = getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%201.txt",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/Version%202.txt",
                getWebdavServletUrl() + "/Global/ds/newnewpage/%40versions/README.txt"
        ));
    }

    /**
     * Test PUT command by drag & drop folder with subfolder from desktop into WebDAV Client
     *
     * @throws IOException
     */
    public void testPutGlobalUploadPageWithSubPageAndTextFiles() throws IOException, DavException {
        for (int i = 1; i < 5; ++i) {
            if (i == 1 || i == 3) {
                String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
                if (i == 3)
                    targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage";
                HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

                int statusCode;
                try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
                    statusCode = response.getStatusLine().getStatusCode();
                }
                assertEquals(statusCode, HttpStatus.SC_CREATED);
            } else {
                String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
                if (i == 4) {
                    targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt";
                }
                HttpPut putMethod = new HttpPut(targetUrl);
                StringEntity requestEntity = new StringEntity("This is newpage");
                if (i == 4) {
                    requestEntity = new StringEntity("This is subpage");
                }
                putMethod.setEntity(requestEntity);

                int statusCode;
                try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
                    statusCode = response.getStatusLine().getStatusCode();
                }
                assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
            }
        }

        String targetUrl = getWebdavServletUrl() + "/Global/ds";

        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/",
                getWebdavServletUrl() + "/Global/ds/%40news/",
                getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                getWebdavServletUrl() + "/Global/ds/Index/",
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/newpage/",
                getWebdavServletUrl() + "/Global/ds/newpage/%40exports/",
                getWebdavServletUrl() + "/Global/ds/newpage/%40versions/",
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/",
                getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt",
                getWebdavServletUrl() + "/Global/ds/newpage/newpage.url"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/",
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/%40exports/",
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/%40versions/",
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt",
                getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.url"
        ));

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/newpage.txt";
        HttpGet getMethod = new HttpGet(targetUrl);
        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "This is newpage";
        Assert.assertEquals(expectedBody, actualBody);

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage/subpage/subpage.txt";
        getMethod = new HttpGet(targetUrl);
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        expectedBody = "This is subpage";
        Assert.assertEquals(expectedBody, actualBody);
    }

    /**
     * Test PUT command by uploading doc file into page exports folder
     *
     * @throws IOException
     */
    public void testPutGlobalUploadPageExportDocAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.doc";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("Upload attachment success");
        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test PUT command by uploading pdf file into page exports folder
     *
     * @throws IOException
     */
    public void testPutGlobalUploadPageExportPdfAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.pdf";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("Upload attachment success");
        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test DELETE command by removing doc file from page exports folder
     *
     * @throws IOException
     */
    public void testDelGlobalRemovePageExportDocAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.doc";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test DELETE command by removing pdf file from page exports folder
     *
     * @throws IOException
     */
    public void testDelGlobalRemovePageExportPdfAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.pdf";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test MOVE command by renaming doc file from page exports folder
     *
     * @throws IOException
     */
    public void testMoveGlobalRenamePageDocExportsAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.doc";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20OverviewS.doc";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);
    }

    /**
     * Test MOVE command by renaming doc file from page exports folder
     *
     * @throws IOException
     */
    public void testMoveGlobalRenamePagePdfExportsAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.pdf";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20OverviewS.pdf";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_PRECONDITION_FAILED, statusCode);
    }

    /**
     * Test PUT command by uploading page url file into page folder
     *
     * @throws IOException
     */
    public void testPutGlobalUploadPageUrlAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.url";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test DELETE command by removing page url file from page folder
     *
     * @throws IOException
     */
    public void testDelGlobalRemovePageUrlAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.url";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test MOVE command by renaming page url file in page folder
     *
     * @throws IOException
     */
    public void testMoveGlobalRenamePageUrlAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20OverviewS.url";
        HttpMove moveMethod = new HttpMove(targetUrl, destinationUrl, false);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(moveMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    /**
     * Test DELETE command by removing page version file from page versions folder
     *
     * @throws IOException
     */
    public void testDelGlobalRemovePageVersionAsFiles() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/%40versions/Version%201.txt";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
    }

    /**
     * Test PUT command by uploading 100 attachment to to a page in one go
     *
     * @throws IOException
     */
    public void testPutGlobalUploadBulkAttachmentAsFile() throws IOException {
        String targetUrl;
        HttpPut putMethod;

        for (int i = 1; i <= 100; ++i) {
            targetUrl = getWebdavServletUrl() + "/Global/ds/Index/testFile" + i + ".txt";
            putMethod = new HttpPut(targetUrl);

            StringEntity requestEntity = new StringEntity("Upload attachment " + i + " success");

            putMethod.setEntity(requestEntity);
            int statusCode;
            try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
                statusCode = response.getStatusLine().getStatusCode();
            }
            assertEquals(HttpServletResponse.SC_CREATED, statusCode);
        }

        for (int i = 1; i <= 100; ++i) {
            targetUrl = getWebdavServletUrl() + "/Global/ds/Index/testFile" + i + ".txt";
            HttpDelete deleteMethod = new HttpDelete(targetUrl);

            try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
                int statusCode = response.getStatusLine().getStatusCode();
                assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
            }
        }

    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test PUT command by updating page content permission
     *
     * @throws IOException
     */
    public void testPutGlobalTestUpdatePagePermissionAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("There is something now");

        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, getForbiddenContext())) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_FORBIDDEN, statusCode);
    }

    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test PUT command by updating personal space description
     *
     * @throws IOException
     */
    public void testPutPersonalUpdateSpaceDescriptionAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/admin.txt";
        HttpPut putMethod = new HttpPut(targetUrl);

        StringEntity requestEntity = new StringEntity("updated admin space description");

        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "updated admin space description";
        Assert.assertEquals(expectedBody, actualBody);
    }

    /**
     * Test PUT command by uploading an attachment to page in Personal Space
     *
     * @throws IOException
     */
    public void testPutPersonalUploadAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile";
        HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpStatus.SC_CREATED, statusCode);

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/testPutPersonalUploadAttachmentAsFile.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "Upload attachment success";
        Assert.assertEquals(expectedBody, actualBody);

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/",
                getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/testPutPersonalUploadAttachmentAsFile.txt",
                getWebdavServletUrl() + "/Personal/~admin/testPutPersonalUploadAttachmentAsFile/testPutPersonalUploadAttachmentAsFile.url"
        ));
    }

    /**
     * Test DELETE command by removing attachment from page in Personal Space
     *
     * @throws IOException
     */
    public void testDelPersonalRemoveAttachmentAsFile() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/test2.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("Upload attachment success");

        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_CREATED, statusCode);

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt",
                getWebdavServletUrl() + "/Personal/~admin/Home/Home.url",
                getWebdavServletUrl() + "/Personal/~admin/Home/test2.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/test2.txt";
        HttpDelete deleteMethod = new HttpDelete(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(deleteMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt",
                getWebdavServletUrl() + "/Personal/~admin/Home/Home.url"
        ));
    }

    /**
     * Test PUT command by updating page content to Personal Space
     *
     * @throws IOException
     */
    public void testPutPersonalUpdatePageContentAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("updated Home page successfully");

        putMethod.setEntity(requestEntity);

        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);

        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "updated Home page successfully";
        Assert.assertEquals(expectedBody, actualBody);
    }

    /**
     * Test PUT command by drag & drop folder with subfolder from desktop into WebDAV Client Personal Space
     *
     * @throws IOException
     */
    public void testPutPersonalUploadPageWithSubPageAndTextFiles() throws IOException, DavException {
        for (int i = 1; i < 5; ++i) {
            if (i == 1 || i == 3) {
                String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
                if (i == 3)
                    targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage";
                HttpMkcol mkColMethod = new HttpMkcol(targetUrl);

                int statusCode;
                try (CloseableHttpResponse response = httpClient.execute(mkColMethod, httpClientContext)) {
                    statusCode = response.getStatusLine().getStatusCode();
                }
                assertEquals(HttpStatus.SC_CREATED, statusCode);
            } else {
                String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt";
                if (i == 4) {
                    targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt";
                }
                HttpPut putMethod = new HttpPut(targetUrl);
                StringEntity requestEntity = new StringEntity("This is newpage");

                if (i == 4) {
                    requestEntity = new StringEntity("This is subpage");
                }
                putMethod.setEntity(requestEntity);

                int statusCode;
                try (CloseableHttpResponse response = httpClient.execute(putMethod, httpClientContext)) {
                    statusCode = response.getStatusLine().getStatusCode();
                }
                assertEquals(HttpServletResponse.SC_NO_CONTENT, statusCode);
            }
        }

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/",
                getWebdavServletUrl() + "/Personal/~admin/%40news/",
                getWebdavServletUrl() + "/Personal/~admin/Home/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/",
                getWebdavServletUrl() + "/Personal/~admin/admin.txt"
        ));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/newpage/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt",
                getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.url"
        ));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage";
        assertPropFindHasResults(targetUrl, asList(
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/%40exports/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/%40versions/",
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt",
                getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.url"
        ));

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/newpage.txt";
        HttpGet getMethod = new HttpGet(targetUrl);

        String actualBody;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        String expectedBody = "This is newpage";
        Assert.assertEquals(expectedBody, actualBody);

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage/subpage/subpage.txt";
        getMethod = new HttpGet(targetUrl);

        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        expectedBody = "This is subpage";
        Assert.assertEquals(expectedBody, actualBody);
    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test PUT command by updating page content permission to personal space
     *
     * @throws IOException
     */
    public void testPutPersonalTestUpdatePagePermissionAsFile() throws IOException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/Home.txt";
        HttpPut putMethod = new HttpPut(targetUrl);
        StringEntity requestEntity = new StringEntity("There is something now");
        putMethod.setEntity(requestEntity);
        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(putMethod, getForbiddenContext())) {
            statusCode = response.getStatusLine().getStatusCode();
        }
        assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
    }

    public void testDownloadPdf() throws IOException {
        String url = getWebdavServletUrl() + "/Global/ds/Index/%40exports/Index.pdf";
        HttpGet getMethod = new HttpGet(url);

        String actualBody;
        int statusCode;
        try (CloseableHttpResponse response = httpClient.execute(getMethod, httpClientContext)) {
            statusCode = response.getStatusLine().getStatusCode();
            InputStream content = response.getEntity().getContent();
            actualBody = IOUtils.toString(content, UTF_8);
        }
        assertEquals(HttpServletResponse.SC_OK, statusCode);
        Assert.assertFalse(actualBody.isEmpty());
    }

    private void assertPropFindHasResults(String targetUrl, List<String> expectedMultiStatusResponseHrefs) throws IOException, DavException {
        HttpPropfind propFindMethod = new HttpPropfind(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        MultiStatus multiStatus;
        MultiStatusResponse[] multiStatusResponses;
        List<String> actualMultiStatusResponseHrefs;
        try (CloseableHttpResponse response = httpClient.execute(propFindMethod, httpClientContext)) {
            multiStatus = propFindMethod.getResponseBodyAsMultiStatus(response);
        }
        multiStatusResponses = multiStatus.getResponses();
        assertNotNull(multiStatusResponses);
        assertEquals(expectedMultiStatusResponseHrefs.size(), multiStatusResponses.length);
        actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

        assertThat(expectedMultiStatusResponseHrefs, hasSize(actualMultiStatusResponseHrefs.size()));
        assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
    }

    private HttpClientContext getForbiddenContext() {
        SystemDefaultCredentialsProvider credentialsProvider = new SystemDefaultCredentialsProvider();
        HttpHost host = new HttpHost(getWebdavServerHostName(),
                getWebdavServerPort(),
                AuthScope.ANY_REALM);
        credentialsProvider.setCredentials(
                new AuthScope(host),
                new UsernamePasswordCredentials("sakura", "sakura")
        );
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicScheme = new BasicScheme();
        authCache.put(host, basicScheme);

        HttpClientContext notPermittedContext = HttpClientContext.create();
        notPermittedContext.setCredentialsProvider(credentialsProvider);
        notPermittedContext.setAuthCache(authCache);
        return notPermittedContext;
    }
}
