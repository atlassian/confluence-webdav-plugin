package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.google.common.collect.ImmutableMap;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;

import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WebdavAttachmentDownloadHeaderTestCase extends AbstractConfluenceResourceTestCase {

    @Test
    public void testAttachmentResourceHasCorrectHeaders() throws Exception {
        AbstractAttachmentResource resource1 = createAttachmentResource("xss.html", "text/html", ImmutableMap.of("Content-Type", "application/x-download",
                "X-Content-Type-Options", "nosniff",
                "Content-Disposition", "attachment;filename=\"xss.html\""));
        assertEquals("did not return expected content type header", "application/x-download", resource1.getContentType());
        OutputContext mockContext = mock(OutputContext.class);
        when(mockContext.hasStream()).thenReturn(true);
        when(mockContext.getOutputStream()).thenReturn(new ByteArrayOutputStream());

        resource1.spool(mockContext);

        verify(mockContext).setContentType("application/x-download");
        assertEquals("X-Content-Type-Options nosniff not found in header:" + mockHttpServletResponse.headers, "nosniff", mockHttpServletResponse.getHeader("X-Content-Type-Options"));
        assertEquals("Content-Disposition not found in header:" + mockHttpServletResponse.headers, "attachment;filename=\"xss.html\"", mockHttpServletResponse.getHeader("Content-Disposition"));
    }

    private PageAttachmentResourceImpl createAttachmentResource(final String fileName, final String contentType, final ImmutableMap<String, String> expectedHeaders) {

        Space space = new Space("ds");
        space.setName("Test Space");
        space.setDescription(new SpaceDescription(space));

        Page page = new Page();
        page.setTitle("Test");
        page.setSpace(space);

        Attachment attachment = new Attachment();
        attachment.setFileName(fileName);
        attachment.setContainer(page);
        attachment.setMediaType(contentType);
        attachment.setLastModificationDate(new Date());

        page.addAttachment(attachment);

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);
        when(attachmentManager.getAttachmentData(attachment)).thenReturn(new ByteArrayInputStream("abcd".getBytes()));

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + attachment.getFileName()
        );
        PageAttachmentResourceImpl resource = new PageAttachmentResourceImpl(
                davResourceLocator,
                davResourceFactory,
                lockManager,
                davSession,
                permissionManager,
                pageManager,
                attachmentSafeContentHeaderGuesser,
                attachmentManager,
                space.getKey(),
                page.getTitle(),
                attachment.getFileName(),
                "fake-user-agent"
        );

        try {
            when(
                    attachmentSafeContentHeaderGuesser.computeAttachmentHeaders(
                            eq(attachment.getMediaType()),
                            any(),
                            eq(attachment.getFileName()),
                            eq("fake-user-agent"),
                            eq(attachment.getFileSize()),
                            eq(false),
                            eq(emptyMap())
                    )
            ).thenReturn(expectedHeaders);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resource;
    }
}
