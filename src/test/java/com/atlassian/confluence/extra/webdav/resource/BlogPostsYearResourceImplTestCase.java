package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

public class BlogPostsYearResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private BlogPostsYearResourceImpl blogPostsYearResource;
    private Space space;

    @Before
    public void initialise() {
        space = new Space("tst");

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/@news/2009"
        );

        blogPostsYearResource = new BlogPostsYearResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, space.getKey(), 2009
        );

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
    }

    @Test
    public void testOnlyBlogPostsOfSpecificYearListedAsChildResources() throws ParseException {
        BlogPost blogPostOfTheYear = new BlogPost();

        blogPostOfTheYear.setSpace(space);
        blogPostOfTheYear.setTitle("blogPostOfTheYear");
        blogPostOfTheYear.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").parse("2009/01/01"));

        when(permissionManager.getPermittedEntities(eq(user), eq(Permission.VIEW), anyList())).thenReturn(
                Arrays.asList(blogPostOfTheYear)
        );

        List<DavResource> davResources = new ArrayList<DavResource>(blogPostsYearResource.getMemberResources());

        assertEquals(1, davResources.size());
        assertEquals("01", davResources.get(0).getDisplayName());
    }
}
