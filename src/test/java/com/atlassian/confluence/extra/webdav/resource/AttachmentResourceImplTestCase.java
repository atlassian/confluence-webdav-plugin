package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AttachmentResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;

    private Space space;
    private Page page;
    private Attachment attachment;

    private PageAttachmentResourceImpl pageAttachmentResourceImpl;

    @Before
    public void initialise() {
        space = new Space("ds");
        space.setName("Test Space");
        space.setDescription(new SpaceDescription(space));

        page = new Page();
        page.setTitle("Test");
        page.setSpace(space);

        attachment = new Attachment();
        attachment.setFileName("Test.doc");
        attachment.setContainer(page);

        page.addAttachment(attachment);

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + attachment.getFileName()
        );

        pageAttachmentResourceImpl = new PageAttachmentResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, pageManager, attachmentSafeContentHeaderGuesser, attachmentManager,
                space.getKey(), page.getTitle(), attachment.getFileName(), "fake-user-agent");
    }

    @Test
    public void testCopyAttachmentToDifferentPageWithDifferentName() throws DavException, IOException {
        final Page destinationPage = new Page();
        final String newAttachmentName = "blog.txt";

        destinationPage.setSpace(space);
        destinationPage.setTitle("Game");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + destinationPage.getTitle() + "/" + newAttachmentName);
        when(pageManager.getPage(destinationPage.getSpaceKey(), destinationPage.getTitle())).thenReturn(destinationPage);
        when(permissionManager.hasCreatePermission(user, destinationPage, Attachment.class)).thenReturn(true);
        when(attachmentManager.getAttachmentData(attachment)).thenReturn(new ByteArrayInputStream(new byte[0]));

        pageAttachmentResourceImpl.copy(davResource, false);

        verify(attachmentManager).saveAttachment(
                argThat(
                        copy -> Objects.equals(copy.getContainer(), destinationPage)
                                && StringUtils.equals(newAttachmentName, copy.getFileName())
                                && attachment.getFileSize() == copy.getFileSize()
                                && StringUtils.equals(attachment.getContentType(), copy.getContentType())
                ),
                any(),
                notNull());
    }

    @Test
    public void testMoveAttachmentToAnotherPageWithDifferentName() throws DavException {
        final Page destinationPage = new Page();
        String newAttachmentName = "blog.txt";

        destinationPage.setSpace(space);
        destinationPage.setTitle("Game");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + destinationPage.getTitle() + "/" + newAttachmentName);
        when(pageManager.getPage(destinationPage.getSpaceKey(), destinationPage.getTitle())).thenReturn(destinationPage);
        when(permissionManager.hasCreatePermission(user, destinationPage, Attachment.class)).thenReturn(true);
        when(permissionManager.hasPermission(user, Permission.REMOVE, attachment)).thenReturn(true);

        pageAttachmentResourceImpl.move(davResource);
        verify(attachmentManager).moveAttachment(attachment, newAttachmentName, destinationPage);
    }

    @Test
    public void testMoveAttachmentToBecomePageContentUpdatesPage() throws DavException {
        final String pageContent = "woohoo";
        final byte[] pageContentBytes = pageContent.getBytes(UTF_8);

        davResource = new PageContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + page.getTitle() + ".txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, pageManager,
                space.getKey(), page.getTitle());


        when(attachmentManager.getAttachmentData(attachment)).thenReturn(new ByteArrayInputStream(pageContentBytes));
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);

        pageAttachmentResourceImpl.move(davResource);

        verify(pageManager).saveContentEntity(
                argThat(
                        ceo -> {
                            String pageContent1 = ceo.getBodyContent().getBody();
                            return StringUtils.equals(pageContent, pageContent1);
                        }
                ),
                eq(page),
                any());
        verify(attachmentManager).removeAttachmentFromServer(attachment);
    }

    @Test
    public void testMoveAttachmentToBecomeSpaceContentUpdatesSpaceDescription() throws DavException {
        final String spaceDescContent = "woohoo";
        final byte[] spaceDescContentBytes = spaceDescContent.getBytes(UTF_8);

        davResource = new SpaceContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + space.getName() + ".txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, spaceManager,
                space.getKey());


        when(attachmentManager.getAttachmentData(attachment)).thenReturn(new ByteArrayInputStream(spaceDescContentBytes));
        when(permissionManager.hasPermission(user, Permission.EDIT, space)).thenReturn(true);

        pageAttachmentResourceImpl.move(davResource);

        verify(spaceManager).saveSpace(
                argThat(
                        space -> StringUtils.equals(spaceDescContent, space.getDescription().getBodyContent().getBody())
                )
        );
        verify(attachmentManager).removeAttachmentFromServer(attachment);
    }

    @Test
    public void testMoveAttachmentToBecomePageDenied() {
        davResource = new PageResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle()
                ),
                davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager, contentJobQueue,
                space.getKey(), page.getTitle()
        );

        try {
            pageAttachmentResourceImpl.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMoveAttachmentToBecomeBlogDenied() {
        davResource = new BlogPostContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/01/blog.txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, userAccessor, pageManager,
                space.getKey(), 2008, 1, 1, "blog.txt"
        ) {
            @Override
            protected Calendar getBlogPostPublishedDate(int yearPublished, int monthPublished, int dayPublished) {
                return Calendar.getInstance();
            }
        };

        try {
            pageAttachmentResourceImpl.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMoveAttachmentToBecomeChildrenOfExportsDirectoryDenied() {
        davResource = new PagePdfExportContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".pdf"
                ),
                davResourceFactory, lockManager, davSession,
                pageManager, pdfExporterService,
                space.getKey(), page.getTitle()
        );


        try {
            pageAttachmentResourceImpl.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMoveAttachmentToBecomeChildrenOfVersionsDirectoryDenied() {

        davResource = new PageVersionContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/Version 1.txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, pageManager,
                space.getKey(), page.getTitle(), 1
        );

        try {
            pageAttachmentResourceImpl.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMoveAttachmentToWithInvalidName() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + "invalid|page|title");

        try {
            pageAttachmentResourceImpl.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testCopyAttachmentToBecomePageDenied() {
        davResource = new PageResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle()
                ),
                davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager, contentJobQueue,
                space.getKey(), page.getTitle()
        );


        try {
            pageAttachmentResourceImpl.copy(davResource, true);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testCopyAttachmentToBecomeBlogDenied() {
        davResource = new BlogPostContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/01/blog.txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, userAccessor, pageManager,
                space.getKey(), 2008, 1, 1, "blog.txt"
        ) {
            @Override
            protected Calendar getBlogPostPublishedDate(int yearPublished, int monthPublished, int dayPublished) {
                return Calendar.getInstance();
            }
        };


        try {
            pageAttachmentResourceImpl.copy(davResource, true);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testCopyAttachmentToBecomeChildrenOfExportsDirectoryDenied() {
        davResource = new PagePdfExportContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".pdf"
                ),
                davResourceFactory, lockManager, davSession,
                pageManager, pdfExporterService,
                space.getKey(), page.getTitle()
        );

        try {
            pageAttachmentResourceImpl.copy(davResource, true);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testCopyAttachmentToBecomeChildrenOfVersionsDirectoryDenied() {
        davResource = new PageVersionContentResourceImpl(
                davLocatorFactory.createResourceLocator(
                        prefix,
                        workspacePath,
                        workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/Version 1.txt"
                ),
                davResourceFactory, lockManager, davSession,
                settingsManager, pageManager,
                space.getKey(), page.getTitle(), 1
        );

        try {
            pageAttachmentResourceImpl.copy(davResource, true);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testCopyAttachmentWithInvalidNameDenied() {

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + "invalid|page|title");

        try {
            pageAttachmentResourceImpl.copy(davResource, true);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testExistsWhenParentAndAttachmentExistsAndUserHasRelevantPermissions() {
        when(permissionManager.hasPermission(eq(user), any(), (Object) any())).thenReturn(true);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);

        assertTrue(pageAttachmentResourceImpl.exists());
    }

    @Test
    public void testDoesNotExistWhenParentDoesNotExist() {
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(null);
        when(permissionManager.hasPermission(eq(user), any(), (Object) any())).thenReturn(true);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);

        assertFalse(pageAttachmentResourceImpl.exists());
    }
}
