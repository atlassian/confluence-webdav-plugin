package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.scheduler.JobRunnerRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ContentJobQueueExecutorTestCase {
    @Mock
    private ContentJobQueue contentJobQueue;

    @Mock
    private JobRunnerRequest jobRunnerRequest;

    @InjectMocks
    private ContentJobQueueExecutor contentJobQueueExecutor;

    @Test
    public void testExecuteTasksOfSpringManagedContentJobQueue() throws Exception {
        contentJobQueueExecutor.runJob(jobRunnerRequest);

        verify(contentJobQueue).executeTasks();
    }
}
