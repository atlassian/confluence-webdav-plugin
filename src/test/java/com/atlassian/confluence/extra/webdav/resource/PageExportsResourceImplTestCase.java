package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PageExportsResourceImplTestCase extends AbstractConfluenceResourceTestCase {
    @Mock
    private DavResource davResource;

    private Space space;
    private Page page;
    private PageExportsResourceImpl pageExportsResourceImpl;

    @Before
    public void initialise() {

        space = new Space("ds");

        page = new Page();
        page.setSpace(space);
        page.setTitle("Test");
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + PageExportsResourceImpl.DISPLAY_NAME);

        pageExportsResourceImpl = new PageExportsResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                webdavSettingsManager, pageManager,
                space.getKey(), page.getTitle());

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
    }

    @Test
    public void testPdfExportUnhiddenOnAdditionAsMember() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".pdf");

        davSession.getResourceStates().hideContentPdfExport(page);
        assertTrue(davSession.getResourceStates().isContentPdfExportHidden(page));

        pageExportsResourceImpl.addMember(davResource, null);

        assertFalse(davSession.getResourceStates().isContentPdfExportHidden(page));
    }

    @Test
    public void testWordExportUnhiddenOnAdditionAsMember() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".doc");

        davSession.getResourceStates().hideContentWordExport(page);
        assertTrue(davSession.getResourceStates().isContentWordExportHidden(page));

        pageExportsResourceImpl.addMember(davResource, null);

        assertFalse(davSession.getResourceStates().isContentWordExportHidden(page));
    }

    @Test
    public void testReadmeUnhiddenOnAdditionAsMember() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + GeneratedResourceReadMeResource.DISPLAY_NAME);

        davSession.getResourceStates().hideContentExportsReadme(page);
        assertTrue(davSession.getResourceStates().isContentExportsReadmeHidden(page));

        pageExportsResourceImpl.addMember(davResource, null);

        assertFalse(davSession.getResourceStates().isContentExportsReadmeHidden(page));
    }

    @Test
    public void testMsWordExportHiddenOnRemove() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".doc");

        pageExportsResourceImpl.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentWordExportHidden(page));
    }

    @Test
    public void testPdfExportHiddenOnRemove() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".pdf");

        pageExportsResourceImpl.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentPdfExportHidden(page));
    }

    @Test
    public void testReadmeHiddenOnDelete() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + GeneratedResourceReadMeResource.DISPLAY_NAME);

        pageExportsResourceImpl.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentExportsReadmeHidden(page));
    }

    @Test
    public void testDoesNotExistIfPageExportsDisabled() {
        when(permissionManager.hasPermission(user, Permission.VIEW, page)).thenReturn(true);

        assertFalse(pageExportsResourceImpl.exists());
        verify(webdavSettingsManager).isContentExportsResourceEnabled();
    }

    @Test
    public void testWordPdfAndReadMeListedAsChildResources() {
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), any())).thenReturn(true);

        List<DavResource> childResources = new ArrayList<>(pageExportsResourceImpl.getMemberResources());

        assertEquals(3, childResources.size());

        PagePdfExportContentResourceImpl pagePdfExportContentResource = (PagePdfExportContentResourceImpl) childResources.get(0);
        assertEquals(page, pagePdfExportContentResource.getPage());

        PageWordExportContentResourceImpl pageWordExportContentResource = (PageWordExportContentResourceImpl) childResources.get(1);
        assertEquals(page, pageWordExportContentResource.getPage());

        GeneratedResourceReadMeResource generatedResourceReadMeResource = (GeneratedResourceReadMeResource) childResources.get(2);
        assertEquals(page, ((PageExportsResourceImpl) generatedResourceReadMeResource.getCollection()).getContentEntityObject());
    }
}
