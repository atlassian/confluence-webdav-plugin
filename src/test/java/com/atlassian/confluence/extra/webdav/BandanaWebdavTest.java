package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.bandana.DefaultBandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.impl.xstream.DefaultConfluenceXStreamManager;
import com.atlassian.confluence.impl.xstream.security.XStreamSecurityConfigurator;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaPersister;
import com.atlassian.confluence.setup.xstream.ConfluenceXStreamManager;
import com.atlassian.confluence.spaces.Space;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class BandanaWebdavTest {

    private static final String SPACE_KEY = "SPACE";
    private static final long CONTENT_ID = 1L;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private BootstrapManager bootstrapManager;
    @Mock
    private Space space;
    @Mock
    private ContentEntityObject content;

    private WebdavSettingsManager webdavSettingsManager;
    private ConfluenceDavSessionStore davSessionStore;

    @Before
    public void setup() {
        when(bootstrapManager.getConfluenceHome()).thenReturn(temporaryFolder.getRoot().getAbsolutePath());

        final ConfluenceBandanaPersister bandanaPersister = new ConfluenceBandanaPersister();
        bandanaPersister.setBootstrapManager(bootstrapManager);
        ConfluenceXStreamManager xStreamManager = new DefaultConfluenceXStreamManager(Collections.emptyMap(),
                this.getClass().getClassLoader(), new XStreamSecurityConfigurator());
        bandanaPersister.setxStreamManager(xStreamManager);

        final BandanaManager bandanaManager = new DefaultBandanaManager(bandanaPersister);

        webdavSettingsManager = new BandanaWebdavSettingsManager(bandanaManager, xStreamManager);
        davSessionStore = new BandanaConfluenceDavSessionStore(bandanaManager);

        when(space.getKey()).thenReturn(SPACE_KEY);
        when(content.getId()).thenReturn(CONTENT_ID);
    }

    @Test
    public void testSaveSettings() {
        final WebdavSettings settings = new WebdavSettings();
        webdavSettingsManager.save(settings);
        assertThat(webdavSettingsManager.getWebdavSettings(), is(settings));
    }

    @Test
    public void testMapSession() {
        final String userName = "viqueen";
        final ConfluenceDavSession session = new ConfluenceDavSession(userName);
        session.getResourceStates().hideSpaceDescription(space);
        session.getResourceStates().hideContentExportsReadme(content);

        davSessionStore.mapSession(session, userName);

        final ConfluenceDavSession restoredSession = davSessionStore.getSession(userName);
        assertThat(restoredSession, notNullValue());
        assertThat(restoredSession.getUserName(), is(session.getUserName()));
        assertTrue(restoredSession.getResourceStates().isSpaceDescriptionHidden(space));
        assertTrue(restoredSession.getResourceStates().isContentExportsReadmeHidden(content));
    }
}
