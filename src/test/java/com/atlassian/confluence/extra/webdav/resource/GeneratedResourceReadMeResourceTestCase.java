package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.spaces.Space;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GeneratedResourceReadMeResourceTestCase extends AbstractConfluenceResourceTestCase {

    private GeneratedResourceReadMeResource generatedResourceReadMeResource;
    private Space space;
    private Page page;
    private String readmeContent;

    @Before
    public void initialise() {
        space = new Space("ds");
        page = new Page();
        page.setSpace(space);
        page.setTitle("test");

        readmeContent = "readmeContent";

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/README.txt"
        );

        generatedResourceReadMeResource = new GeneratedResourceReadMeResource(
                davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager) {
            @Override
            protected String getReadMeContent() {
                return readmeContent;
            }
        };

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

    }

    @Test
    public void testReadMeContentEncodedWithSpecifiedCharset() throws IOException {
        byte[] expectedOutputBytes = readmeContent.getBytes("UTF-8");

        Settings globalSettings = new Settings();
        globalSettings.setDefaultEncoding("UTF-8");


        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        OutputContext outputContext = mock(OutputContext.class);

        InputStream in = null;
        ByteArrayOutputStream out = null;

        try {
            in = generatedResourceReadMeResource.getContent();
            out = new ByteArrayOutputStream();


            when(outputContext.hasStream()).thenReturn(true);
            when(outputContext.getOutputStream()).thenReturn(out);

            generatedResourceReadMeResource.spool(outputContext);

            assertEquals(
                    readmeContent,
                    new String(out.toByteArray(), "UTF-8")
            );

            verify(outputContext).setContentLength(expectedOutputBytes.length);
            verify(outputContext).setContentType(generatedResourceReadMeResource.getContentTypeBase());
            verify(outputContext).setModificationTime(anyLong());
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    @Test
    public void testDisplayName() {
        assertEquals("README.txt", generatedResourceReadMeResource.getDisplayName());
    }

    @Test
    public void testCreationTimeEqualsToRelatedPageCreationTime() {
        page.setCreationDate(new Date());

        assertEquals(
                page.getCreationDate().getTime(),
                generatedResourceReadMeResource.getCreationtTime()
        );
    }

    @Test
    public void testDoesNotExistIfHiddenAsChildOfExportsDirectory() {
        when(webdavSettingsManager.isContentExportsResourceEnabled()).thenReturn(true);
        davSession.getResourceStates().hideContentExportsReadme(page);

        assertFalse(generatedResourceReadMeResource.exists());
    }

    @Test
    public void testDoesNotExistIfHiddenAsChildOfVersionsDirectory() {
        when(webdavSettingsManager.isContentVersionsResourceEnabled()).thenReturn(true);
        davSession.getResourceStates().hideContentExportsReadme(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/README.txt"
        );

        generatedResourceReadMeResource = new GeneratedResourceReadMeResource(
                davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);


        when(webdavSettingsManager.isContentVersionsResourceEnabled()).thenReturn(false);
        assertFalse(generatedResourceReadMeResource.exists());
    }

    @Test
    public void testExistsWhenExportsDirectoryAsParentExists() {
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), any())).thenReturn(true);
        when(webdavSettingsManager.isContentExportsResourceEnabled()).thenReturn(true);
        assertTrue(generatedResourceReadMeResource.exists());
    }

    @Test
    public void testExistsWhenVersionsDirectoryAsParentExists() {
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), (Object) any())).thenReturn(true);
        when(webdavSettingsManager.isContentVersionsResourceEnabled()).thenReturn(true);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/README.txt"
        );

        generatedResourceReadMeResource = new GeneratedResourceReadMeResource(
                davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);

        assertTrue(generatedResourceReadMeResource.exists());
    }
}
