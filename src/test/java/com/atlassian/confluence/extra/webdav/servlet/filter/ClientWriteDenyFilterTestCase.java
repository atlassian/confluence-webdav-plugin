package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.WebdavSettingsManager;
import junit.framework.TestCase;
import org.apache.jackrabbit.webdav.DavMethods;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientWriteDenyFilterTestCase extends TestCase {
    private ClientWriteDenyFilter clientWriteDenyFilter;

    @Mock
    private WebdavSettingsManager webdavSettingsManager;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private FilterChain filterChain;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        clientWriteDenyFilter = new ClientWriteDenyFilter(webdavSettingsManager);
    }

    @Override
    protected void tearDown() throws Exception {
        webdavSettingsManager = null;
        httpServletRequest = null;
        httpServletResponse = null;
        filterChain = null;
        super.tearDown();
    }

    public void testErrorSentIfHttpPutAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_PUT);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public void testErrorSentIfHttpCopyAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_COPY);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public void testErrorSentIfHttpDeleteAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_DELETE);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public void testErrorSentIfHttpMkcolAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_MKCOL);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public void testErrorSentIfHttpMoveAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_MOVE);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    public void testErrorNotSentIfNotHttpWriteOperationAndClientIsBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_GET);
        when(webdavSettingsManager.isClientInWriteBlacklist(anyString())).thenReturn(true);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(filterChain).doFilter(httpServletRequest, httpServletResponse);
    }

    public void testErrorNotSentIfHttpWriteOperationAndClientIsNotBlacklisted() throws ServletException, IOException {
        when(httpServletRequest.getMethod()).thenReturn(DavMethods.METHOD_PUT);

        clientWriteDenyFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(filterChain).doFilter(httpServletRequest, httpServletResponse);
    }
}
