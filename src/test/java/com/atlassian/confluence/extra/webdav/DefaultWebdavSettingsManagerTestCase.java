package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.impl.xstream.DefaultConfluenceXStreamManager;
import com.atlassian.confluence.impl.xstream.security.XStreamSecurityConfigurator;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.xstream.ConfluenceXStreamManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultWebdavSettingsManagerTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private DefaultWebdavSettingsManager defaultWebdavSettingsManager;

    @Mock
    private BandanaManager bandanaManager;
    @Mock
    private CacheManager cacheManager;

    private Cache<Object, Object> cache;

    @Before
    public void setUp() {
        ConfluenceXStreamManager xStreamManager = new DefaultConfluenceXStreamManager(Collections.emptyMap(),
                this.getClass().getClassLoader(), new XStreamSecurityConfigurator());
        defaultWebdavSettingsManager = new DefaultWebdavSettingsManager(bandanaManager, cacheManager, xStreamManager);
        cache = new WebDavCache<>("com.atlassian.confluence.extra.webdav.settings.global");
        when(cacheManager.getCache("com.atlassian.confluence.extra.webdav.settings")).thenReturn(cache);
    }

    @Test
    public void testWebdavSettingsReadFromCacheIfNotModified() {
        assertNotNull(defaultWebdavSettingsManager.getWebdavSettings());
        /* Now, if we call getWebdavSetting again, bandana should not be invoked */
        assertNotNull(defaultWebdavSettingsManager.getWebdavSettings());

        verify(bandanaManager, times(1)).getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "com.atlassian.confluence.extra.webdav-2.0.settings"
        );
    }

    @Test
    public void testWebdavSettingsReadFromCacheIsUpdatedOnModification() {
        assertTrue(defaultWebdavSettingsManager.getWebdavSettings().isContentExportsResourceEnabled());

        WebdavSettings newSettings = new WebdavSettings();

        newSettings.setContentExportsResourceEnabled(false);
        newSettings.setContentUrlResourceEnabled(false);
        newSettings.setContentVersionsResourceEnabled(false);

        defaultWebdavSettingsManager.save(newSettings);

        assertFalse(defaultWebdavSettingsManager.isContentExportsResourceEnabled());
        assertFalse(defaultWebdavSettingsManager.isContentUrlResourceEnabled());
        assertFalse(defaultWebdavSettingsManager.isContentVersionsResourceEnabled());

        verify(bandanaManager, times(1)).getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "com.atlassian.confluence.extra.webdav-2.0.settings"
        );
        verify(bandanaManager, times(1)).setValue(
                eq(ConfluenceBandanaContext.GLOBAL_CONTEXT),
                eq("com.atlassian.confluence.extra.webdav-2.0.settings"),
                isA(WebdavSettings.class)
        );
    }

    @Test
    public void testClientBlacklistedAccordingToRegularExpression() {
        WebdavSettings webdavSettings = defaultWebdavSettingsManager.getWebdavSettings();

        webdavSettings.setExcludedClientUserAgentRegexes(singleton("(?i)webdavfs/\\d\\.\\d(\\.\\d)*"));
        defaultWebdavSettingsManager.save(webdavSettings);

        assertTrue(defaultWebdavSettingsManager.isClientInWriteBlacklist("wEbDAvfS/1.0"));
        assertTrue(defaultWebdavSettingsManager.isClientInWriteBlacklist("WEBDAVFS/1.1.1"));

        assertFalse(defaultWebdavSettingsManager.isClientInWriteBlacklist("Microsoft-Min-Redir/1.0"));
    }

    @Test
    public void testCachedSettingsLoadedByAnotherVersionOfThePluginRepopulatedFromBandanaWhenRetrieved() {
        String settingsKey = "com.atlassian.confluence.extra.webdav.settings.global";

        cache.put(settingsKey, "I am actually a String and I cannot be cast to a WebdavSettings. Muahahahaha!");
        defaultWebdavSettingsManager.getWebdavSettings();

        assertThat(cache.get(settingsKey), instanceOf(WebdavSettings.class));
        verify(bandanaManager).getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, "com.atlassian.confluence.extra.webdav-2.0.settings");
    }

}
