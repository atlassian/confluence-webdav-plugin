package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import org.apache.jackrabbit.webdav.DavResource;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class BlogPostsMonthResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private BlogPostsMonthResourceImpl blogPostsMonthResource;
    private Space space;

    @Before
    public void initialise() {
        space = new Space("tst");

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/@news/2009/01"
        );

        blogPostsMonthResource = new BlogPostsMonthResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession, userAccessor,
                permissionManager, spaceManager, pageManager, space.getKey(), 2009, 1
        );

        when(userAccessor.getConfluenceUserPreferences(user)).thenReturn(confluenceUserPreferences);
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
    }

    @Test
    public void testDisplayNameShowsDoubleDigitsIfMonthIsEarlierThanOctober() {
        assertEquals("01", blogPostsMonthResource.getDisplayName());
    }

    @Test
    public void testBlogPostOfSpecificMonthListedAsChildResources() throws ParseException {
        BlogPost blogOfTheMonth = new BlogPost();
        blogOfTheMonth.setSpace(space);
        blogOfTheMonth.setTitle("blogOfTheMonth");
        blogOfTheMonth.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").parse("2009/01/31"));

        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());
        when(permissionManager.getPermittedEntities(eq(user), eq(Permission.VIEW), anyList())).thenReturn(
                singletonList(blogOfTheMonth)
        );

        List<DavResource> davResources = new ArrayList<>(blogPostsMonthResource.getMemberResources());

        assertEquals(1, davResources.size());
        assertEquals(31, ((BlogPostsDayResourceImpl) davResources.get(0)).getDayPublished());

        verify(pageManager).getBlogPosts(eq(space.getKey()), isA(Calendar.class), eq(Calendar.MONTH));
    }
}
