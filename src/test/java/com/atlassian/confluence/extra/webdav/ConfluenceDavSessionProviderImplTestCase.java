package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.user.impl.DefaultUser;
import org.apache.struts2.ServletActionContext;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConfluenceDavSessionProviderImplTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private WebdavRequest webdavRequest;
    @Mock
    private HttpSession httpSession;
    @Mock
    private UserAccessor userAccessor;
    @Mock
    private ConfluenceDavSessionStore confluenceDavSessionStore;
    @Mock
    private SecurityConfig securityConfig;
    @Mock
    private Authenticator authenticator;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private ServletContext servletContext;

    protected ConfluenceUser user;

    private ConfluenceDavSession davSession;
    private ConfluenceDavSessionProviderImpl confluenceDavSessionProviderImpl;

    @Before
    public void setUp() {
        when(httpServletRequest.getSession()).thenReturn(httpSession);
        when(httpSession.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(SecurityConfig.STORAGE_KEY)).thenReturn(securityConfig);
        when(securityConfig.getAuthenticator()).thenReturn(authenticator);

        ServletActionContext.setRequest(httpServletRequest);
        ServletActionContext.setResponse(httpServletResponse);

        user = new ConfluenceUserImpl(new DefaultUser("admin"));
        davSession = new ConfluenceDavSession(user.getName());

        confluenceDavSessionProviderImpl =
                new ConfluenceDavSessionProviderImpl(userAccessor, confluenceDavSessionStore) {
                    @Override
                    protected void setConfluenceDavSessionIntoHttpSession(
                            HttpServletRequest httpServletRequest,
                            ConfluenceDavSession confluenceDavSession) {
                        super.setConfluenceDavSessionIntoHttpSession(httpServletRequest, davSession);
                    }
                };
    }

    @Test
    public void testAttachSessionWithoutExistingSession() throws DavException, AuthenticatorException {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(webdavRequest.getHeader("Authorization")).thenReturn("Basic YWRtaW46YWRtaW4=");
        when(confluenceDavSessionStore.getSession(user.getName())).thenReturn(null);
        when(authenticator.login(any(), any(), eq("admin"), eq("admin"))).thenReturn(true);
        when(userAccessor.getUserByName("admin")).thenReturn(user);

        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);


            verify(confluenceDavSessionStore).mapSession(isA(ConfluenceDavSession.class), eq(user.getName()));
            verify(httpSession).setAttribute(eq(ConfluenceDavSession.class.getName()), isA(ConfluenceDavSession.class));

            verify(webdavRequest).setDavSession(
                    argThat(
                            ConfluenceDavSession::isCurrentlyBeingUsed
                    )
            );
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test
    public void testAttachSessionWithExistingSession() throws DavException, AuthenticatorException {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(webdavRequest.getHeader("Authorization")).thenReturn("Basic YWRtaW46YWRtaW4=");
        when(userAccessor.getUserByName("admin")).thenReturn(user);
        when(authenticator.login(any(), any(), eq("admin"), eq("admin"))).thenReturn(true);
        when(confluenceDavSessionStore.getSession(user.getName())).thenReturn(davSession);

        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);

            assertTrue(davSession.isCurrentlyBeingUsed());

            verify(confluenceDavSessionStore).mapSession(davSession, user.getName());
            verify(httpSession).setAttribute(ConfluenceDavSession.class.getName(), davSession);
            verify(webdavRequest).setDavSession(davSession);
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    /**
     * Test attachSession() method using Windows Client with Malformed Authentication Header Via Network Drive
     */
    @Test
    public void testAttachSessionWithMalformedAuthenticationHeaderByWindowsClientViaNetworkDrive() {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(webdavRequest.getHeader("Authorization")).thenReturn("Basic Og==");

        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_UNAUTHORIZED, e.getErrorCode());
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    /**
     * Test for WBDV-130. OOo 2.3 doesn't send a User-Agent header. It would previously throw a NullPointerException
     *
     * @throws DavException Thrown if there is a problem attaching the a {@link org.apache.jackrabbit.webdav.DavSession} to the current
     *                      HTTP request.
     */
    @Test
    public void testNoUserAgentHeader() throws DavException, AuthenticatorException {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(webdavRequest.getHeader("Authorization")).thenReturn("Basic YWRtaW46YWRtaW4=");
        when(authenticator.login(any(), any(), eq("admin"), eq("admin"))).thenReturn(true);
        when(userAccessor.getUserByName("admin")).thenReturn(user);
        when(confluenceDavSessionStore.getSession(user.getName())).thenReturn(davSession);
        when(webdavRequest.getHeader("User-Agent")).thenReturn(null);

        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test(expected = DavException.class)
    public void testMissingAuthorizationHeaderRaisesDavException() throws DavException {
        try {
            when(webdavRequest.getSession()).thenReturn(httpSession);
            when(webdavRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION)).thenReturn(null);
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test
    public void testMalformedAuthorizationHeaderRaisesDavException() {
        try {
            when(webdavRequest.getSession()).thenReturn(httpSession);
            when(webdavRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION)).thenReturn("Basic ");
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
            fail();
        } catch (DavException de) {
            /* Success */
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test
    public void testDavSessionInHttpSessionPreferredForAttachmentIfAvailable() throws DavException {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute(ConfluenceDavSession.class.getName())).thenReturn(davSession);

        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
            verify(webdavRequest).setDavSession(davSession);
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test
    public void testReauthenticateIfDavSessionInHttpSessionIsLoadedByDifferentClassLoader() throws AuthenticatorException {
        try {
            when(webdavRequest.getSession()).thenReturn(httpSession);
            when(httpSession.getAttribute(ConfluenceDavSession.class.getName())).thenReturn(new Object());
            when(webdavRequest.getHeader("Authorization")).thenReturn("Basic YWRtaW46YWRtaW4=");
            when(userAccessor.getUserByName(user.getName())).thenReturn(user);

            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
        } catch (DavException de) {
            verify(authenticator).login(any(), any(), anyString(), anyString());
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
        }
    }

    @Test
    public void testDetachSessionCleanUp() {
        when(webdavRequest.getDavSession()).thenReturn(davSession);

        confluenceDavSessionProviderImpl.releaseSession(webdavRequest);

        assertFalse(davSession.isCurrentlyBeingUsed());
        assertNull(AuthenticatedUserThreadLocal.get());
        verify(webdavRequest).setDavSession(null);
    }

    @Test
    public void testAttachSessionWithAuthenticatedUser() throws DavException {

        AuthenticatedUserThreadLocal.set(user);

        when(webdavRequest.getSession()).thenReturn(httpSession);


        try {
            confluenceDavSessionProviderImpl.attachSession(webdavRequest);
        } finally {
            confluenceDavSessionProviderImpl.releaseSession(webdavRequest);
            AuthenticatedUserThreadLocal.set(null);
        }

        verify(webdavRequest, never()).getHeader("Authorization");
        verify(confluenceDavSessionStore, never()).getSession(user.getName());

        verify(confluenceDavSessionStore).mapSession(isA(ConfluenceDavSession.class), eq(user.getName()));
        verify(httpSession).setAttribute(ConfluenceDavSession.class.getName(), davSession);
        verify(webdavRequest).setDavSession(isA(ConfluenceDavSession.class));
    }

    @Test
    public void testAuthenticateWithColonInPassword() throws AuthenticatorException, DavException {
        when(webdavRequest.getSession()).thenReturn(httpSession);
        when(webdavRequest.getHeader(WebdavConstants.HEADER_AUTHORIZATION)).thenReturn(
                "Basic YWRtaW46Zm9vOmJhcg=="
        );
        when(authenticator.login(
                eq(httpServletRequest),
                eq(httpServletResponse),
                eq("admin"),
                eq("foo:bar")
        )).thenReturn(true);

        assertTrue(confluenceDavSessionProviderImpl.attachSession(webdavRequest));
    }
}
