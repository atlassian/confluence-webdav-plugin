package com.atlassian.confluence.extra.webdav;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.extra.webdav.listener.PageRestoreListener;
import com.atlassian.confluence.extra.webdav.resource.PageVersionContentResourceImpl;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class DefaultConfluenceDavSessionStoreTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private DefaultConfluenceDavSessionStore defaultConfluenceDavSessionStore;

    private PageRestoreListener pageRestoreListener;

    @Mock
    private CacheManager cacheManager;
    @Mock
    private EventPublisher eventPublisher;

    private Cache<Object, Object> cache;

    private Page page;
    private Attachment attachment;
    private User user;
    private ConfluenceDavSession confluenceDavSession;

    @Before
    public void setUp() {
        cache = new WebDavCache<>("com.atlassian.confluence.extra.webdav.sessions");

        when(cacheManager.getCache("com.atlassian.confluence.extra.webdav.sessions")).thenReturn(cache);

        defaultConfluenceDavSessionStore = new DefaultConfluenceDavSessionStore(cacheManager);
        pageRestoreListener = new PageRestoreListener(eventPublisher, defaultConfluenceDavSessionStore);

        attachment = new Attachment("test.txt", "text/plain", 0, StringUtils.EMPTY);
        page = new Page();
        page.setSpace(new Space("ds"));

        page.addAttachment(attachment);
        attachment.setContainer(page);

        user = new DefaultUser("admin");
        confluenceDavSession = new ConfluenceDavSession(user.getName());
    }

    @Test
    public void testContentUnhiddenWhenPageIsRestored() {
        assertNotNull(pageRestoreListener);

        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());

        /* Hide content properties */
        ResourceStates resourceStates = confluenceDavSession.getResourceStates();

        resourceStates.hideContent(page);
        resourceStates.hideContentMarkup(page);
        resourceStates.hideContentUrl(page);
        resourceStates.hideContentExports(page);
        resourceStates.hideContentExportsReadme(page);
        resourceStates.hideContentPdfExport(page);
        resourceStates.hideContentWordExport(page);
        resourceStates.hideContentVersions(page);
        resourceStates.hideContentVersionsReadme(page);
        resourceStates.hideContentVersionText(page, PageVersionContentResourceImpl.DISPLAY_NAME_PREFIX + 1 + PageVersionContentResourceImpl.DISPLAY_NAME_SUFFIX);
        resourceStates.hideAttachment(attachment);


        pageRestoreListener.handleEvent(
                new PageRestoreEvent(
                        this, page
                )
        );

        assertFalse(resourceStates.isContentHidden(page));
        assertFalse(resourceStates.isContentMarkupHidden(page));
        assertFalse(resourceStates.isContentUrlHidden(page));

        assertFalse(resourceStates.isContentExportsHidden(page));
        assertFalse(resourceStates.isContentExportsReadmeHidden(page));
        assertFalse(resourceStates.isContentPdfExportHidden(page));
        assertFalse(resourceStates.isContentWordExportHidden(page));

        assertFalse(resourceStates.isContentVersionsHidden(page));
        assertFalse(resourceStates.isContentVersionsReadmeHidden(page));
        assertFalse(resourceStates.isContentVersionTextHidden(page, PageVersionContentResourceImpl.DISPLAY_NAME_PREFIX + 1 + PageVersionContentResourceImpl.DISPLAY_NAME_SUFFIX));

        assertFalse(resourceStates.isAttachmentHidden(attachment));

    }

    @Test
    public void testMappedSessionCanBeRetrievedWithOwningUser() {
        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());
        assertThat(
                defaultConfluenceDavSessionStore.getSession(user.getName()),
                is(confluenceDavSession)
        );
    }

    @Test
    public void testMappedSessionNotRetrievableUsingNonOwningUser() {
        User nonOwningUser = new DefaultUser("foobar");

        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());

        assertNull(defaultConfluenceDavSessionStore.getSession(nonOwningUser.getName()));
    }

    @Test
    public void testSessionGetsInvalidatedIfTimedOutAndNotBeingUsed() throws InterruptedException {
        defaultConfluenceDavSessionStore = new DefaultConfluenceDavSessionStore(cacheManager, 1); /* Really small timeout */
        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());

        Thread.sleep(500);

        defaultConfluenceDavSessionStore.invalidateExpiredSessions();

        assertNull(defaultConfluenceDavSessionStore.getSession(user.getName()));
    }

    @Test
    public void testSessionDoesNotGetInvalidatedIfTimedOutButBeingUsed() throws InterruptedException {
        confluenceDavSession.setCurrentlyBeingUsed(true);

        defaultConfluenceDavSessionStore = new DefaultConfluenceDavSessionStore(cacheManager, 1); /* Really small timeout */
        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());

        Thread.sleep(500);

        defaultConfluenceDavSessionStore.invalidateExpiredSessions();

        assertThat(defaultConfluenceDavSessionStore.getSession(user.getName()), is(confluenceDavSession));
    }

    @Test
    public void testSessionDoesNotGetInvalidatedIfUnusedButNotTimedOut() {
        confluenceDavSession.setCurrentlyBeingUsed(false);

        defaultConfluenceDavSessionStore = new DefaultConfluenceDavSessionStore(cacheManager, Long.MAX_VALUE); /* Really big timeout */
        defaultConfluenceDavSessionStore.mapSession(confluenceDavSession, user.getName());

        defaultConfluenceDavSessionStore.invalidateExpiredSessions();

        assertThat(defaultConfluenceDavSessionStore.getSession(user.getName()), is(confluenceDavSession));
    }

    @Test
    public void testCachedSessionLoadedByAnotherVersionOfThePluginPurgedWhenRetrieved() {
        String userName = "admin";

        cache.put(userName, "I am actually a String and I cannot be cast to a ConfluenceDavSession. Muahahahaha!");
        defaultConfluenceDavSessionStore.getSession(userName);

        assertNull(cache.get(userName));
    }

    @Test
    public void testCachedSessionLoadedByAnotherVersionOfThePluginPurgedOnInvalidate() {
        String userName = "admin";

        cache.put(userName, "I am actually a String and I cannot be cast to a ConfluenceDavSession. Muahahahaha!");
        defaultConfluenceDavSessionStore.invalidateExpiredSessions();
        assertNull(cache.get(userName));
    }

}
