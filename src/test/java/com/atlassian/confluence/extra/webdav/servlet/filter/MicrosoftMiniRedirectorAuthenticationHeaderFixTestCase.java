package com.atlassian.confluence.extra.webdav.servlet.filter;

import junit.framework.TestCase;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MicrosoftMiniRedirectorAuthenticationHeaderFixTestCase extends TestCase {
    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private FilterConfig filterConfig;

    @Mock
    private FilterChain filterChain;

    private Filter filterFix;

    public void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        filterFix = new MicrosoftMiniRedirectorAuthenticationHeaderFix();
        filterFix.init(filterConfig);
    }

    @Override
    protected void tearDown() throws Exception {
        servletRequest = null;
        filterConfig = null;
        filterChain = null;
        super.tearDown();
    }

    public void testAuthorizationHeaderNotRewrittenIfClientIsNotMsMiniRedirector() throws ServletException, IOException {
        when(servletRequest.getHeader("User-Agent")).thenReturn("WebDAVFS");

        filterFix.doFilter(servletRequest, null, filterChain);
        verify(filterChain).doFilter(servletRequest, null);
    }

    public void testAuthorizationHeaderNotRewrittenServletRequestNotAnInstanceOfHttpServletRequest() throws ServletException, IOException {
        ServletRequest servletRequest = mock(ServletRequest.class);

        filterFix.doFilter(servletRequest, null, filterChain);
        verify(filterChain).doFilter(servletRequest, null);
    }

    public void testAuthorizationHeaderNotRewrittenIfItDoesNotContainBackslashInUserName() throws ServletException, IOException {
        when(servletRequest.getHeader("User-Agent")).thenReturn("Microsoft-WebDAV-MiniRedir/x.x");
        when(servletRequest.getHeader("Authorization")).thenReturn(
                "Basic "
                        + new String(
                        Base64.encodeBase64("admin:admin".getBytes(UTF_8)),
                        UTF_8
                )
        );

        filterFix.doFilter(servletRequest, null, filterChain);
        verify(filterChain).doFilter(servletRequest, null);
    }

    public void testAuthorizationHeaderRewrittenIfItContainsBackslashInUserName() throws ServletException, IOException {
        final String expectedHeader = "Basic " + new String(
                Base64.encodeBase64("admin:admin".getBytes(UTF_8)),
                UTF_8
        );

        when(servletRequest.getHeader("User-Agent")).thenReturn("Microsoft-WebDAV-MiniRedir/x.x");
        when(servletRequest.getHeader("Authorization")).thenReturn(
                "Basic "
                        + new String(
                        Base64.encodeBase64("localhost\\admin:admin".getBytes(UTF_8)),
                        UTF_8
                )
        );


        filterFix.doFilter(servletRequest, null, filterChain);

        verify(filterChain).doFilter(
                argThat(
                        (ArgumentMatcher<HttpServletRequest>) request -> StringUtils.equals(expectedHeader,
                                request.getHeader("Authorization"))
                ),
                eq(null));
    }

}
