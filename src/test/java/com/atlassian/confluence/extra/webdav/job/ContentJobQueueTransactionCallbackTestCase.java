package com.atlassian.confluence.extra.webdav.job;

import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Exercises {@link com.atlassian.confluence.extra.webdav.job.ContentJobQueueTransactionCallbackTestCase}
 */
public class ContentJobQueueTransactionCallbackTestCase extends TestCase {
    @Mock
    private ContentJob testContentJob;

    private ContentJobQueueTransactionCallback subject;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        subject = new ContentJobQueueTransactionCallback(testContentJob);
    }

    @Override
    protected void tearDown() throws Exception {
        subject = null;
        testContentJob = null;
        super.tearDown();
    }

    public void testContentJobIsExecuted() throws Exception {
        assertEquals(testContentJob, subject.doInTransaction());

        verify(testContentJob).execute();
    }

}
